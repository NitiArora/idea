﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Utility.Consts
{
    public class FilteredBy
    {
        public const int CreatedLast = 1;
        public const int MostLiked = 2;
        public const int MostDisliked = 3;
        public const int MostComments = 4;
        public const int CreatedFirst = 5;
    }
    public class PagingSize
    {
        public const int Page = 6;
        public const int DefaultIdeas = 15;
        public const int DefaultDashboardIdeas = 10;
       
    }
    public class IdeaPage
    {
        public const int AddIdea = 1;
        public const int ViewIdea = 2;
        public const int ManageIdea = 3;
        public const int IdeaDetail = 4;
    }
    public class ThreadCountStatic
    {
        public static int ThreadNumber = 0;
       
    }
    public class IdeaCode
    {
        public static string Code = "IDN";

    }
    public class FileType
    {
        public const string Jpeg = ".jpeg";
        public const string Jpg  = ".jpg";
        public const string Png  = ".png";
        public const string Gif  = ".gif";
    }
    public class Role
    {
        public const int SuperAdminId = 1;
        public const int ReviewPanelId = 2;
      
    }
    public class IdeaStatus
    {
        public const int Pending = 1;
        public const int Review = 2;
        public const int Approved = 3;
        public const int Rejected = 4;
        public const int InProgress = 5;
        public const int Done = 6;
        public const int Live = 7;
        

    }
    
    public class CacheDetail
    {
        public const int CacheCleanDuration = 180;
        public const string Cache_IdeaWatchList = "Cache_IdeaWatchList";

        public const string Cache_PlatformType = "Cache_PlatformType";
        public const string Cache_PageName = "Cache_PageName";
        public const string Cache_ProductType = "Cache_ProductType";
        public const string Cache_BusinessObjective = "Cache_BusinessObjective";
        public const string Cache_BusinessImpact = "Cache_BusinessImpact";
        public const string Cache_Category = "Cache_Category";
        public const string Cache_Location = "Cache_Location";
        public const string Cache_Replicated = "Cache_Replicated";
        public const string Cache_RelatedTo = "Cache_RelatedTo";
    }
}
