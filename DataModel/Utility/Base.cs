﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Caching;
using System.Web.Configuration;
using System.Xml.Linq;

namespace DataModel.Utility
{
    public class Base
    {
        public static string GetTime(DateTime dt)
        {
            var ts = DateTime.Now.Subtract(dt);
            return GetTime(ts);
        }
        public static List<string> SuperAdmin()
        {
            List<string> superAdmin = new List<string>();
            superAdmin = FetchEmailIds();
            return superAdmin;
        }

        private static List<string> FetchEmailIds()
        {
            string pathValue = GetConfigValue("SuperAdminEmailIds");
            List<string> emailIds = new List<string>();
            string toEmailIds = pathValue;
            for (int i = 0; i < toEmailIds.Split(',').Length; i++)
            {
                emailIds.Add(toEmailIds.Split(',')[i].Trim());
            }
            return emailIds;
        }

        public static string GetConfigValue(string key)
        {
            try
            {
                var xDoc = XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web.config"));
                var appSettings = xDoc.Root.Element("appSettings");
                var result = (string)appSettings.Elements("add").First(x => (string)x.Attribute("key") == key).Attribute("value");
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static DateTime GetCurrentIndianDateTIme()
        {
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("India Standard Time"));
            return indianTime;
        }

        public static string GetTime(TimeSpan ts, bool addAgo = true)
        {
            string ago = (addAgo) ? " ago" : "";
            if (ts.TotalMinutes < 1)
            {
                if (addAgo)
                {
                    return "just now";
                }
                else
                {
                    return "no time";
                }
            }

            if (ts.TotalHours < 1)
            {
                return (int)ts.TotalMinutes + " minutes " + ago;
            }

            if (ts.TotalDays < 1)
            {
                return (int)ts.TotalHours + " hours " + ago;
            }

            return (int)ts.TotalDays + " days " + ago;
        }

        #region Cache
        public static void AddCache(string cacheName, object obj)
        {
            HttpRuntime.Cache.Add(cacheName, obj, null, DateTime.Now.AddMinutes(Convert.ToDouble(Consts.CacheDetail.CacheCleanDuration)), Cache.NoSlidingExpiration, CacheItemPriority.AboveNormal, new CacheItemRemovedCallback(OnCacheExpiredCallBack));

        }
        public static void OnCacheExpiredCallBack(string key, object value, CacheItemRemovedReason reason)
        {
            //throw new NotImplementedException();
        }
        public static Object GetMyCachedItem(String CacheKeyName)
        {
            return HttpRuntime.Cache[CacheKeyName] as Object;
        }
        public static void ResetCache()
        {
             HttpRuntime.Cache.Remove(DataModel.Utility.Consts.CacheDetail.Cache_IdeaWatchList);
          
        }
        public static string BaseImageUrl
        {

            get
            {
                return GetConfigValue("VirtualImagePath");
            }
        }     
        #endregion
    }
}
