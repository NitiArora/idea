﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Utility
{
    public static class Helper
    {
        #region Method for Send Email

        public static bool SendMailToList(List<string> emailIds, string body)
        {
            try
            {
                SendEmail(body, "IdeaWatch", emailIds);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void SendExceptionInEmail(string body)
        {
            SendEmail(body, "Exception in IdeaWatch", FetchEmailIds());
        }

        #endregion

        #region Private Methods

        private static void SendEmail(string body, string subject, List<string> emailIds)
        {
            try
            {
                //var smtpClient = new SmtpClient(DTIslandSetting.GetConfigValue("MailServer"));
                var smtpClient = new SmtpClient("smtp.gmail.com");
                //smtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtpClient.Port = 587;
                smtpClient.Credentials = new NetworkCredential("rana.singh@fareportal.com", "test123");
                smtpClient.EnableSsl = true;
                var msg = new MailMessage();
                msg.From = new MailAddress(DTIslandSetting.GetConfigValue("FromEmailId"));
                msg.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                foreach (var to in emailIds)
                {
                    msg.To.Add(to);
                }
                msg.Subject = subject;
                msg.IsBodyHtml = true;
                msg.Body = body;
                smtpClient.Send(msg);
            }
            catch (Exception ex)
            {
                Logger.WriteLogInfo("Error in SendEmail Method: " + ex);
            }
        }

        private static List<string> FetchEmailIds()
        {
            List<string> emailIds = new List<string>();
            string toEmailIds = DTIslandSetting.GetConfigValue("EmailIdsForException");
            for (int i = 0; i < toEmailIds.Split(',').Length; i++)
            {
                emailIds.Add(toEmailIds.Split(',')[i]);
            }
            return emailIds;
        }

        #endregion
    }
}
