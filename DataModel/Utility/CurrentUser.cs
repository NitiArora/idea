﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DataModel.Utility
{
   public  class CurrentUser
    {
        public static BO.UserDetail CurrentUserDetail
        {
            get
            {
                if (HttpContext.Current.Session["CurrentUserDetail"] != null)
                    return (BO.UserDetail)HttpContext.Current.Session["CurrentUserDetail"];
                else
                    return new BO.UserDetail();
            }
            set
            {
                HttpContext.Current.Session["CurrentUserDetail"] = value;
            }
        }
    }
}
