﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.Utility
{
    public sealed class Logger
    {
        # region Private / Public variables
        private static object loggingLockMutex = new object();// use this object to Lock log file while writing to it
        private static readonly Int16 MESSAGES_TO_KEEP_IN_MEMORY = Int16.Parse(System.Configuration.ConfigurationManager.AppSettings["LogMessagesToKeepInMemory"]);

        private const string LINE_SEPERATOR = "\r\n-----------------------------------------------\r\n";

        private static string LogPath = System.Configuration.ConfigurationManager.AppSettings["LogPath"];
        private static System.Collections.Queue LastException = new System.Collections.Queue(MESSAGES_TO_KEEP_IN_MEMORY);
        # endregion

        private Logger()
        {

        }

        # region Method for writing log file
        public static void WriteLog(string logMsg_)
        {
            try
            {
                WriteLogInfo(logMsg_);
            }
            catch
            {
                //Nothing To Do
            }
        }

        public static void WriteLogInfo(string logMsg_)
        {
            try
            {
                // time when we writing log in file.
                string errorTime;
                errorTime = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                LastException.Enqueue(errorTime + logMsg_);
                LastException.Enqueue(LINE_SEPERATOR);

                if (LastException.Count >= MESSAGES_TO_KEEP_IN_MEMORY)
                    WriteExceptiontoFile();
            }
            catch
            {
                //Nothing To Do
            }
        }

        // writes the message to file.
        private static void WriteExceptiontoFile()
        {
            StringBuilder messageList = new StringBuilder();

            while (LastException.Count > 0)
            {
                messageList.Append(LastException.Dequeue());
            }

            StreamWriter logWriter = null;

            string fileUrl = getURL();
            lock (loggingLockMutex)// Added lock to avoid Exception
            {
                try
                {
                    logWriter = File.AppendText(fileUrl);
                    logWriter.WriteLine(messageList.ToString());
                }
                finally
                {
                    if (logWriter != null)
                    {
                        logWriter.Flush();
                        logWriter.Close();
                        logWriter = null;
                    }
                }
            }
        }

        // method will return the fileURL to create log file after reading value from config and datetime from Cache
        // it will also create directory if found not exits.
        private static string getURL()
        {
            string fileUrl = LogPath + "FPIdeasWatchAppLog_" + DateTime.Now.ToString("MMddyyyy_hhtt") + ".txt";

            if (!Directory.Exists(LogPath))
            {
                Directory.CreateDirectory(LogPath);
            }
            return fileUrl;
        }
        # endregion
    }
}
