﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace DataModel.Utility
{
    public static class DTIslandSetting
    {
        public static string GetConfigValue(string key)
        {
            try
            {
                var xDoc = XDocument.Load(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Web.config"));
                var appSettings = xDoc.Root.Element("appSettings");
                var result = (string)appSettings.Elements("add").First(x => (string)x.Attribute("key") == key).Attribute("value");
                return result;
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Exception in GetConfigValue(string key): " + ex);
                return null;
            }
        }
    }
}
