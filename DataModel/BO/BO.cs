﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace DataModel.BO
{
    public class BO
    {
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }
    public class UserDetail : Model.User
    {
        public bool IsSuperAdmin { get; set; }
        public bool IsReviewPanelMember { get; set; }  

    }
    public class IdeasWatchList
    {
        public int TotalRecords { get; set; }
        public List<IdeasWatchDetail> IdeasWatchDetailList { get; set; }
    }
    public class ChartView
    {
        public int Id { get; set; }
        public int Value { get; set; }
        public string Name { get; set; }
    }
    public class IdeaStatusDetail 
    { 
        public int IdeaStatusId { get; set; }
        //public List<Model.IdeaStatu> IdeaStatusList { get; set; }
        public List<SelectListItem> IdeaStatusList { get; set; }
       
    }
    public class IdeasWatchDetail : Model.IdeasWatch
    {
        public string UserName { get; set; }
        public string CreatedByEmail { get; set; }
        public string keys { get; set; }
        public List<Model.IdeaKey> KeyList { get; set; }
        public List<Model.IdeaLikeDislike> IdeaLikeDislikeList { get; set; }
        public List<int> KeyIdList { get; set; }
        public List<DataModel.Model.FileAttachment> AttachmentList { get; set; }
        public List<string> LikedUserList { get; set; }
        public List<string> UnlikedUserList { get; set; }
        public List<CommentDetail> CommentDetailList { get; set; }
        public bool IsLikedByCuurentUser { get; set; }
        public bool IsEditPermission { get; set; }
        public bool IsDislikedByCuurentUser { get; set; }
        public string FilesToBeUploaded { get; set; }
        public string StatusChangedByName { get; set; }
        public int AttachmentCount { get; set; }
        public int BusinessImpactId { get; set; }
        public int BusinessObjectiveId { get; set; }
        public int PageNameId { get; set; }
        public int ProductTypeId { get; set; }
        public int PlatformTypeId { get; set; }
        public int LocationId { get; set; }
        //public int ReplicatedId { get; set; }
        //public int RelatedToId { get; set; }
        public string Status { get; set; }
        public string ReplicatedName { get; set; }
        public string RelatedToName { get; set; }
        public string CategoryName { get; set; }
        //public string CurrentScenario { get; set; }
        //public string BenifitIdea { get; set; }
        public List<SelectListItem> PageNameList { get; set; }
        public List<SelectListItem> ProductTypeList { get; set; }
        public List<SelectListItem> PlatformTypeList { get; set; }
        public List<SelectListItem> BusinessObjectiveList { get; set; }
        public List<SelectListItem> BusinessImpactList { get; set; }
        public List<SelectListItem> CategoryList { get; set; }
        public List<SelectListItem> LocationList { get; set; }
        public List<SelectListItem> ReplicatedList { get; set; }
        public List<SelectListItem> RelatedToList { get; set; }

        public Model.PageName PageNameModel { get; set; }
        public Model.ProductType ProductTypeModel { get; set; }
        public Model.PlatformType PlatformTypeModel { get; set; }
        public Model.BusinessObjective BusinessObjectiveModel { get; set; }
        public Model.BusinessImpact BusinessImpactModel { get; set; }
        public Model.Category CategoryModel { get; set; }
        public Model.Location LocationModel { get; set; }
        public Model.Replicated ReplicatedModel { get; set; }
        public Model.RelatedTo RelatedToModel { get; set; }  
        public int DataForPageId { get; set; }

        public int ParentId { get; set; }
        public int CurrentStatusId { get; set; }
        public string CurrentStatusName { get; set; }
        public int CurrentStepId { get; set; }

        public List<NextStatus> NextStatusList { get; set; }
      

    }
    public class NextStatus   
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }

    }
    public class CommentDetail : Model.IdeaComment
    {
        public string UserName { get; set; }
        public string ImageUrl { get; set; }
        public List<CommentReplyDetail> CommentReplyList { get; set; }

    }
    public class CommentReplyDetail : Model.CommentReply
    {
        public string UserName { get; set; }
        public string ImageUrl { get; set; }

    }
    public class KeyCloud 
    {
        public int KeyId { get; set; }
        public string text { get; set; }
        public float weight { get; set; }      
        public string link { get; set; }

    }

    #region Service Retun Type

    public abstract class ServiceReturnType
    {
        public class ServiceResult<T>
        {
            public bool IsSessionExpired { get; set; }

            public bool IsSuccessful { get; set; }

            public T Result { get; set; }

            public string Message { get; set; }
        }
        public static ServiceResult<bool> SuccessBoolResult
        {
            get
            {
                return new ServiceResult<bool>
                {
                    Message = "Operation Successful",
                    IsSuccessful = true,
                    Result = true
                };
            }
        }
        public static ServiceResult<bool> FailureBoolResult
        {
            get
            {
                return new ServiceResult<bool>
                {
                    Message = "Operation Unsuccessful",
                    IsSuccessful = false,
                    Result = false
                };
            }
        }
        public static ServiceResult<T> FailureWithDataResult<T>(object data)
        {
            return new ServiceResult<T>
            {
                IsSuccessful = false,
                Message = "Failed",
                Result = data is T ? (T)data : default(T)
            };
        }
        public static ServiceResult<T> SuccessWithDataResult<T>(object data)
        {
            return new ServiceResult<T>
            {
                IsSuccessful = true,
                Message = "Success",
                Result = data is T ? (T)data : default(T)
            };
        }

    }

    #endregion
    #region Privilege
    public class UserPrivilegeDetail
    {
        public bool IsDelete{ get; set; }
    }
    public class CurrentUserPrivilege
    {
        public static UserPrivilegeDetail Privileges
        {
            get
            {
                if (HttpContext.Current.Session["UserPrivilegeDetail"] != null)
                    return (UserPrivilegeDetail)HttpContext.Current.Session["UserPrivilegeDetail"];
                else
                    return new UserPrivilegeDetail();
            }
            set
            {
                HttpContext.Current.Session["UserPrivilegeDetail"] = value;
            }
        }
    }
    #endregion

}
