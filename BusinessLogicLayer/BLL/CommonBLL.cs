﻿using DataModel.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BusinessLogicLayer.BLL
{
    public class CommonBLL
    {
        public static List<DataModel.Model.IdeaStatu> GetStatusList()
        {
            return DataAccessLayer.DAL.CommonDAL.StatusList();
        }

        public static List<SelectListItem> GetSelectListItemStatus()
        {
            var result = GetStatusList();
            List<SelectListItem> selectListItem = new List<SelectListItem>();
            //selectListItem.Add(new SelectListItem { Text = "-- Select --", Value = "0" });
            foreach (var item in result)
            {
                selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.IdeaStatusId.ToString() });
            }
            return selectListItem;
        }

        public static string GetHtmlTemplate(string templatePath)
        {
            string body = string.Empty;
            try
            {
                using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath(templatePath)))
                {
                    body = reader.ReadToEnd();
                }
                return body;
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Error in GetHtmlTemplate() : " + ex);
                return string.Empty;
            }
        }
        public static string PopulateHtmlFormattedBody(string subject, string description)
        {
            string body = string.Empty;
            try
            {
                body = GetHtmlTemplate("~/Template/EmailTemplate.html");
                body = body.Replace("@subject", subject);
                body = body.Replace("@Description", description);
                return body;
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Error in PopulateHtmlFormattedBody() : " + ex);
                return string.Empty;
            }
        }

    }

}
