﻿using DataModel.Utility.Consts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.BLL
{
  public  class ManageIdeaBLL
    {
      public static DataModel.BO.ServiceReturnType.ServiceResult<DataModel.BO.IdeasWatchDetail> ChangeIdeaStatus(int ideaId, int ideaStatusId)
        {
            return DataAccessLayer.DAL.ManageIdeaDAL.ChangeIdeaStatus(ideaId, ideaStatusId);
        }
        public static DataModel.BO.IdeasWatchList GetIdeasListForManageIdea(int statusId)
        {

            DataModel.BO.IdeasWatchList ideasWatchList = IdeasWatch.GetFilteredIdeaViewModelListForManageIdea(statusId);
            var listIdea = ideasWatchList.IdeasWatchDetailList;
            if (!DataModel.Utility.CurrentUser.CurrentUserDetail.IsSuperAdmin && DataModel.Utility.CurrentUser.CurrentUserDetail.IsReviewPanelMember)
            {
                if (statusId!=DataModel.Utility.Consts.IdeaStatus.Pending)              
                listIdea = (from a in listIdea where a.StatusChangedBy == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId select a).ToList();
            }
            ideasWatchList.IdeasWatchDetailList = listIdea;
            ideasWatchList.TotalRecords = ideasWatchList.IdeasWatchDetailList.Count;
            return ideasWatchList;

        }
    }
}
