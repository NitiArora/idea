﻿using DataModel.BO;
using DataModel.Utility.Consts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.BLL
{
    public class Dashboard
    {
        public static List<ChartView> GetIdeaStatusForChartData()
        {

            return DataAccessLayer.DAL.Dashboard.GetIdeaStatusData();
        }
        public static DataModel.BO.IdeasWatchList GetIdeasListForDashboard(int statusId)
        {

            DataModel.BO.IdeasWatchList ideasWatchList = IdeasWatch.GetFilteredIdeaViewModelListForManageIdea(statusId);
            var listIdea = ideasWatchList.IdeasWatchDetailList;
            if (!DataModel.Utility.CurrentUser.CurrentUserDetail.IsSuperAdmin && !DataModel.Utility.CurrentUser.CurrentUserDetail.IsReviewPanelMember)
            { listIdea = (from a in listIdea where a.CreatedBy == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId select a).ToList(); }
            ideasWatchList.IdeasWatchDetailList = listIdea.Take(PagingSize.DefaultDashboardIdeas).ToList();
            ideasWatchList.TotalRecords = listIdea.Count;
            return ideasWatchList;

        }

        public static DataModel.BO.IdeasWatchList GetIdeasForIdeaList(int statusId)
        {

            DataModel.BO.IdeasWatchList ideasWatchList = IdeasWatch.GetFilteredIdeaViewModelListForManageIdea(statusId);
            var listIdea = ideasWatchList.IdeasWatchDetailList;
            if (!DataModel.Utility.CurrentUser.CurrentUserDetail.IsSuperAdmin && !DataModel.Utility.CurrentUser.CurrentUserDetail.IsReviewPanelMember)
            { listIdea = (from a in listIdea where a.CreatedBy == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId select a).ToList(); }
            ideasWatchList.IdeasWatchDetailList = listIdea;
            ideasWatchList.TotalRecords = ideasWatchList.IdeasWatchDetailList.Count;
            return ideasWatchList;

        }
    }
}
