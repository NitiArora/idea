﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLayer.BLL
{
   public class User
    {
       public static DataModel.BO.UserDetail AddUser(string UserName, string imageUrl)
       {
           return DataAccessLayer.DAL.User.AddUser(UserName, imageUrl);
       }
       public static bool AssignRole(int[] userId, int roleId)
       {
           return DataAccessLayer.DAL.User.AssignRole(userId, roleId);
       }
       public static bool RemoveRole(int userId)
       {
           return DataAccessLayer.DAL.User.RemoveRole(userId);
       }
       public static List<DataModel.BO.UserDetail> GetUserOfReviewPanel()
       {
           return DataAccessLayer.DAL.User.GetUserOfReviewPanel();
       }
       public static List<string> GetEmailListOfReviewPanel()
       {
           return DataAccessLayer.DAL.User.GetEmailListOfReviewPanel();
       }
       public static List<DataModel.BO.UserDetail> GetUserWithoutReviewPanel()
       {
           var users=DataAccessLayer.DAL.User.GetUserWithoutReviewPanel();
           var superAdmins=DataModel.Utility.Base.SuperAdmin();
           var userForReviewPanel=users.Where(a=>!superAdmins.Contains(a.UserName)).ToList();
           return userForReviewPanel;
       }
    }
}
