﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.Utility.Consts;
using System.Web.Mvc;
namespace BusinessLogicLayer.BLL
{
   public class IdeasWatch
    {
       public static int AddKey(string keyName)
       {
          // return DataAccessLayer.DAL.IdeasWatch.AddIdea(keyName);
           return 1;
       }
       public static DataModel.BO.ServiceReturnType.ServiceResult<DataModel.BO.IdeasWatchDetail> AddIdeaWatch(DataModel.BO.IdeasWatchDetail ideaWatchDeatil)
       {
           //DataModel.Model.IdeasWatch ideasWatch = new DataModel.Model.IdeasWatch();
           //ideasWatch.Title = ideaWatchDeatil.Title;
           //ideasWatch.Description = ideaWatchDeatil.Description;
           //ideasWatch.IsBug = ideaWatchDeatil.IsBug;
           //ideasWatch.CreatedBy = DataModel.Utility.CurrentUser.CurrentUserDetail.UserId;
           //ideasWatch.CreatedOn = DataModel.Utility.Base.GetCurrentIndianDateTIme();// DateTime.Now;
           var sr = DataAccessLayer.DAL.IdeasWatch.AddIdeaWatch(ideaWatchDeatil);
           if (sr.IsSuccessful)
           {
               DataAccessLayer.DAL.IdeasWatch.AddIdeasWatchIdeakey(ideaWatchDeatil.keys, sr.Result.IdeasWatchId);
           }

           return sr;
       }
       public static int AddIdeaLikeDislike(DataModel.Model.IdeaLikeDislike obj)
       {
           return DataAccessLayer.DAL.IdeasWatch.AddIdeaLikeDislike(obj);
       }
       public static int AddIdeaWatchAssIdeaKey(DataModel.Model.IdeasWatch_Ideakey obj)
       {
           return DataAccessLayer.DAL.IdeasWatch.AddIdeaWatchAssIdeaKey(obj);
       }
       public static int AddIdeaComments(DataModel.Model.IdeaComment obj)
       {
           return DataAccessLayer.DAL.IdeasWatch.AddIdeaComments(obj);
       }
       public static int AddIdeaAttachement(DataModel.Model.FileAttachment obj)
       {
           return DataAccessLayer.DAL.IdeasWatch.AddIdeaAttachement(obj);
       }
       public static DataModel.BO.IdeasWatchList GetFilteredIdeaViewModelList( int filterBy, int statusId,int categoryId=0, string keys="")
       {
           
           List<DataModel.BO.IdeasWatchDetail> ideas = DataAccessLayer.DAL.IdeasWatch.GetIdeasWatchDetailList();
           if (statusId > 0)
           {
               if(statusId==DataModel.Utility.Consts.IdeaStatus.Approved)
               {
                   ideas = ideas.Where(a => a.IdeaStatusId == statusId || a.ParentId == DataModel.Utility.Consts.IdeaStatus.Approved).ToList();
               }
               else
               {
                   ideas = ideas.Where(a => a.IdeaStatusId == statusId ).ToList();
               }
              
           }
           if (categoryId > 0)
           {
               ideas = ideas.Where(a => a.IdeaCategoryId == categoryId).ToList();
           }
           if (!string.IsNullOrEmpty(keys))
           {
               string[] keysArray = keys.Split(',');
               List<int> keyids = new List<int>();
               foreach (var key in keysArray)
               {
                   if (!string.IsNullOrEmpty(key))                  
                   keyids.Add(Convert.ToInt32(key));
               }
               if (keysArray.Count() > 0)
               {
                   ideas = (from a in ideas where a.KeyIdList.Any(b => keyids.Contains(b)) select a).ToList();
               }
           }

           List<DataModel.BO.IdeasWatchDetail> filteredIdeas = new List<DataModel.BO.IdeasWatchDetail>();
           if (filterBy == FilteredBy.CreatedLast)
           {
               filteredIdeas = ideas.OrderByDescending(b => b.CreatedOn).ToList();
           }
           else if (filterBy == FilteredBy.CreatedFirst)
           {
               filteredIdeas = ideas.OrderBy(b => b.CreatedOn).ToList();
           }
           else if (filterBy == FilteredBy.MostLiked)
           {
               filteredIdeas = ideas.OrderByDescending(b => b.LikedUserList.Count).ToList();
           }
           else if (filterBy == FilteredBy.MostComments)
           {
               filteredIdeas = ideas.OrderByDescending(b => b.CommentDetailList.Count).ToList();
           }

           
           DataModel.BO.IdeasWatchList ideasWatchList = new DataModel.BO.IdeasWatchList();
           ideasWatchList.TotalRecords = ideas.Count;          
           ideasWatchList.IdeasWatchDetailList = filteredIdeas;
           return ideasWatchList;
       }
       public static DataModel.BO.IdeasWatchList GetFilteredIdeaViewModelListForManageIdea(int statusId)
       {

           List<DataModel.BO.IdeasWatchDetail> ideas = DataAccessLayer.DAL.IdeasWatch.GetIdeasWatchDetailList();
           if (statusId > 0)
           {
               ideas = ideas.Where(a => a.IdeaStatusId == statusId).OrderByDescending(b => b.CreatedOn).ToList();

           }
           DataModel.BO.IdeasWatchList ideasWatchList = new DataModel.BO.IdeasWatchList();
           ideasWatchList.TotalRecords = ideas.Count;
           ideasWatchList.IdeasWatchDetailList = ideas;
           return ideasWatchList;
       }
       public static DataModel.BO.IdeasWatchList GetFilteredIdeaBasedOnUserId()
       {
           DataModel.BO.IdeasWatchList ideasWatchList = new DataModel.BO.IdeasWatchList();
           ideasWatchList = GetFilteredIdeaViewModelList(FilteredBy.CreatedLast, 0,0, "");
           ideasWatchList.IdeasWatchDetailList = ideasWatchList.IdeasWatchDetailList.Where(a => a.CreatedBy == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId).ToList();
           return ideasWatchList;
       }
       public static DataModel.BO.IdeasWatchDetail GetIdeasWatchDetailByIdeaCode(string ideaCode)
       {
           return DataAccessLayer.DAL.IdeasWatch.GetIdeasWatchDetail(0, ideaCode);
       }
       public static DataModel.BO.IdeasWatchDetail GetIdeasWatchDetail(int ideaWaatchId,string ideaCode ="0")
       {
           return DataAccessLayer.DAL.IdeasWatch.GetIdeasWatchDetail(ideaWaatchId, ideaCode);
       }
       public static List<DataModel.BO.CommentDetail> GetCommentsByIdeaWatchId(int ideaWaatchId)
       {
           return DataAccessLayer.DAL.IdeasWatch.GetCommentsByIdeaWatchId(ideaWaatchId);
       }
       public static List<DataModel.BO.CommentReplyDetail> GetCommentReplyByCommentId(int commentId)
       {
           return DataAccessLayer.DAL.IdeasWatch.GetCommentReplyByCommentId(commentId);
       }
       public static List<DataModel.Model.IdeaKey> GetAllKey()
       {
           return DataAccessLayer.DAL.IdeasWatch.GetAllKey();
       }
       public static int GetTotalNumberOfIdeas()
       {
           return DataAccessLayer.DAL.IdeasWatch.GetTotalNumberOfIdeas();
       }
       public static DataModel.Model.FileAttachment GetFileAttachment(int attachmentId)
       {
           return DataAccessLayer.DAL.IdeasWatch.GetFileAttachment(attachmentId);
       }
       public static bool DeleteIdea(int ideaWatchId)
       {
           return DataAccessLayer.DAL.IdeasWatch.DeleteIdea(ideaWatchId);
       }
       public static DataModel.BO.ServiceReturnType.ServiceResult<int> UpdateIdeaWatch(DataModel.BO.IdeasWatchDetail ideaWatchDeatil)
       {
           return DataAccessLayer.DAL.IdeasWatch.UpdateIdeaWatch(ideaWatchDeatil);
          
       }
       public static int AddCommentReply(DataModel.Model.CommentReply obj)
       {
           return DataAccessLayer.DAL.IdeasWatch.AddCommentReply(obj);
       }
       public static List<DataModel.BO.KeyCloud> GetKeyCloudList()
       {
           return DataAccessLayer.DAL.IdeasWatch.GetKeyCloudList();
       }
       public static string GetKeyName(int keyId)
       {
           return DataAccessLayer.DAL.IdeasWatch.GetKeyName(keyId);
       }

       public static List<DataModel.Model.PlatformType> GetPlatformType()
       {
           return DataAccessLayer.DAL.IdeasWatch.GetPlatformType();
       }
       public static List<SelectListItem> GetSelectListItemPlatformType()
       {
           var result= DataAccessLayer.DAL.IdeasWatch.GetPlatformType();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           selectListItem.Add(new SelectListItem { Text = "-- Select --", Value = "0" });
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.PlatformTypeId.ToString()});
           }
           return selectListItem;
       }

       public static List<DataModel.Model.PageName> GetPageName()
       {
           return DataAccessLayer.DAL.IdeasWatch.GetPageName();
       }
       public static List<SelectListItem> GetSelectListItemPageName()
       {
           var result = DataAccessLayer.DAL.IdeasWatch.GetPageName();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           selectListItem.Add(new SelectListItem { Text = "-- Select --", Value = "0" });
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.PageNameId.ToString() });
           }
           return selectListItem;
       }

       public static List<DataModel.Model.ProductType> GetProductType()
       {
           return DataAccessLayer.DAL.IdeasWatch.GetProductType();
       }
       public static List<SelectListItem> GetSelectListItemProductType()
       {
           var result = DataAccessLayer.DAL.IdeasWatch.GetProductType();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           selectListItem.Add(new SelectListItem { Text = "-- Select --", Value = "0" });
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.ProductTypeId.ToString() });
           }
           return selectListItem;
       }

       public static List<DataModel.Model.BusinessObjective> GetBusinessObjective()
       {
           return DataAccessLayer.DAL.IdeasWatch.GetBusinessObjective();
       }
       public static List<SelectListItem> GetSelectListItemBusinessObjective()
       {
           var result = DataAccessLayer.DAL.IdeasWatch.GetBusinessObjective();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           selectListItem.Add(new SelectListItem { Text = "-- Select --", Value = "0" });
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.BusinessObjectiveId.ToString() });
           }
           return selectListItem;
       }

       public static List<DataModel.Model.Category> GetCategory()
       {
           return DataAccessLayer.DAL.IdeasWatch.GetCategory();
       }

       public static List<SelectListItem> GetSelectListItemCategory()
       {
           var result = DataAccessLayer.DAL.IdeasWatch.GetCategory();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           selectListItem.Add(new SelectListItem { Text = "-- Select --", Value = "0" });
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.CategoryId.ToString() });
           }
           return selectListItem;
       }

       public static List<SelectListItem> GetSelectListItemLocation()
       {
           var result = DataAccessLayer.DAL.IdeasWatch.GetLocation();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           selectListItem.Add(new SelectListItem { Text = "-- Select --", Value = "" });
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.LocationId.ToString() });
           }
           return selectListItem;
       }

       public static List<SelectListItem> EditSelectListItemLocation()
       {
           var result = DataAccessLayer.DAL.IdeasWatch.GetLocation();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.LocationId.ToString() });
           }
           return selectListItem;
       }

       public static List<SelectListItem> GetSelectListItemReplicated()
       {
           var result = DataAccessLayer.DAL.IdeasWatch.GetReplicated();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           selectListItem.Add(new SelectListItem { Text = "-- Select --", Value = "" });
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.ReplicatedId.ToString() });
           }
           return selectListItem;
       }

       public static List<SelectListItem> GetSelectListItemRelatedTo()
       {
           var result = DataAccessLayer.DAL.IdeasWatch.GetRelatedTo();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           selectListItem.Add(new SelectListItem { Text = "-- Select --", Value = "" });
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.RelatedToId.ToString() });
           }
           return selectListItem;
       }

       public static List<SelectListItem> GetSelectListItemCategoryNotUsed()
       {
           var result = DataAccessLayer.DAL.IdeasWatch.GetCategoryNotInUse();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.CategoryId.ToString() });
           }
           return selectListItem;
       }

       public static List<DataModel.Model.BusinessImpact> GetBusinessImpact()
       {
           return DataAccessLayer.DAL.IdeasWatch.GetBusinessImpact();
       }
       public static List<SelectListItem> GetSelectListItemBusinessImpact()
       {
           var result = DataAccessLayer.DAL.IdeasWatch.GetBusinessImpact();
           List<SelectListItem> selectListItem = new List<SelectListItem>();
           selectListItem.Add(new SelectListItem { Text = "-- Select --", Value = "0" });
           foreach (var item in result)
           {
               selectListItem.Add(new SelectListItem { Text = item.Name, Value = item.BusinessImpactId.ToString() });
           }
           return selectListItem;
       }
      

       public static bool DeleteComment(int commentId)
       {
           return DataAccessLayer.DAL.IdeasWatch.DeleteComment(commentId);
       }
       public static bool DeleteCommentReply(int commentReplyId)
       {
           return DataAccessLayer.DAL.IdeasWatch.DeleteCommentReply(commentReplyId);
       }

       public static bool DeleteCategory(int categoryId)
       {
           return DataAccessLayer.DAL.IdeasWatch.DeleteCategory(categoryId);
       }

       public static int AddNewCategory(DataModel.Model.Category obj)
       {
           return DataAccessLayer.DAL.IdeasWatch.AddNewCategory(obj);
       }
     
       
    }
}
