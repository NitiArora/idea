﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FPIdeasWatch.Startup))]
namespace FPIdeasWatch
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
