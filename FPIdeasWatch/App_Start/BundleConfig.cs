﻿using System.Web;
using System.Web.Optimization;

namespace FPIdeasWatch
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {

            bundles.Add(new ScriptBundle("~/bundles/scripts").Include(
               // "~/Scripts/jquery-{version}.js",
                // needed for drag/move events in fullcalendar
              //  "~/Scripts/jquery-ui-{version}.js",
              "~/Scripts/bootstrap.js",
               // "~/Scripts/bootstrap-modal.js",               
                // "~/Scripts/respond.js",
              "~/Scripts/common.js"
                ));

            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*",
                         "~/Scripts/jquery.unobtrusive-ajax.js"
                        ));


            bundles.Add(new ScriptBundle("~/bundles/ViewIdeaWatch").Include(
                        "~/Scripts/ViewIdeaWatch.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/sweetalert").Include(
                        "~/Scripts/sweetalert.min.js"
                        ));
            bundles.Add(new ScriptBundle("~/bundles/Dashboard").Include(
                       "~/Scripts/IdeaWatch/Dashboard.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/multiSelect").Include(
                      "~/Scripts/IdeaWatch/multiSelect.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/ManageUser").Include(
                       "~/Scripts/IdeaWatch/ManageUser.js"
                       ));
            bundles.Add(new ScriptBundle("~/bundles/ManageIdea").Include(
                       "~/Scripts/IdeaWatch/ManageIdea.js"
                       ));
            bundles.Add(new ScriptBundle("~/bundles/AddIdea").Include(
                       "~/Scripts/IdeaWatch/AddIdea.js"
                       ));
           
            bundles.Add(new ScriptBundle("~/bundles/HighCharts").Include(
                     "~/Scripts/Highcharts-4.0.1/js/highcharts.js"
                     ));


            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            //bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            //          "~/Scripts/bootstrap.js",
            //          "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/Site.css",
                      "~/Content/font.css",
                      "~/Content/sweetalert.css"
                      ));
        }
    }
}
