﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FPIdeasWatch
{
    public class MvcApplication : System.Web.HttpApplication
    {
       // static Thread keepAliveThread = new Thread(KeepAlive);
        private void Application_BeginRequest(Object source, EventArgs e)
        {


            List<string> allowedIps = new List<string>();
            allowedIps.Add("70.42.217.10");
            allowedIps.Add("207.99.71.228");
            allowedIps.Add("216.214.181.210");
            allowedIps.Add("125.19.58.0");
            allowedIps.Add("115.113.178.0");
            allowedIps.Add("115.113.51.138");
            allowedIps.Add("66.93.145.236");
            allowedIps.Add("24.120.134.5");
            allowedIps.Add("125.19.58.1");
            allowedIps.Add("125.19.58.2");
            allowedIps.Add("14.141.65.43");
            allowedIps.Add("115.248.1.1");
            allowedIps.Add("125.19.58.3");
            allowedIps.Add("115.113.178.1");
            allowedIps.Add("14.141.65.41");
            allowedIps.Add("14.141.65.42");
            allowedIps.Add("14.141.65.43");
            allowedIps.Add("14.141.65.44");
            allowedIps.Add("14.141.65.45");
            allowedIps.Add("14.141.65.46");

            HttpApplication application = (HttpApplication)source;
            HttpContext context = application.Context;

            string ip = context.Request.UserHostAddress;
            string ipFromHeader = context.Request.Headers["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ipFromHeader))
                ipFromHeader = context.Request.Headers["X-Forwarded-For"];
            if (string.IsNullOrEmpty(ipFromHeader))
                ipFromHeader = "115.113.51.138";

            if (context.Request.Path != "/Content/404.html")
            {
                if (context.Request.IsLocal == false && allowedIps.Contains(ip) == false && allowedIps.Contains(ipFromHeader) == false)
                {
                    context.Response.StatusCode = 404;
                    context.Response.StatusDescription = "Not Found";
                    context.ApplicationInstance.CompleteRequest();
                }
            }
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();

        }
        protected void Application_Start()
        {
          //  DataModel.Utility.Consts.ThreadCountStatic.ThreadNumber = 1;
        //  keepAliveThread.Start();
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_End()
        {
          // keepAliveThread.Abort();
        }
        static void KeepAlive()
        {
            var urlToActive = System.Configuration.ConfigurationManager.AppSettings["URLToActiveSite"];
            while (true)
            {
                WebRequest req = WebRequest.Create(urlToActive);
              
                try
                {
                    req.GetResponse();
                    Thread.Sleep(60000);
                }
                catch (ThreadAbortException ex)
                {
                    
                }
            }
        }
    }
}
