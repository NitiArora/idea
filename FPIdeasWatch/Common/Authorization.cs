﻿using FPIdeasWatch.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FPIdeasWatch.Common
{
    public class Authorization
    {
    }
    public class IsSessionExpired : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (DataModel.Utility.CurrentUser.CurrentUserDetail.UserName == null || DataModel.Utility.CurrentUser.CurrentUserDetail.UserId==0)
            {

                var httpContext = filterContext.HttpContext;
                var request = httpContext.Request;
                var response = httpContext.Response;
                if (request.IsAjaxRequest())
                {
                    response.StatusCode = 401;
                    response.SuppressFormsAuthenticationRedirect = true;
                    response.End();
                }
                else
                {
                    filterContext.Result =
                        new RedirectToRouteResult(
                        new RouteValueDictionary{{ "controller", "Account" },
                                          { "action", "Login" }

                                         });

                   // base.HandleUnauthorizedRequest(filterContext);
                    //  filterContext.Result = new RedirectToRouteResult()
                }


            }

        }
       
    }

    public class IsSuperAdmin : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (DataModel.Utility.CurrentUser.CurrentUserDetail.UserName != null && DataModel.Utility.CurrentUser.CurrentUserDetail.IsSuperAdmin != true)
            {

                var httpContext = filterContext.HttpContext;
                var request = httpContext.Request;
                var response = httpContext.Response;
                if (request.IsAjaxRequest())
                {
                    response.StatusCode = 401;
                    response.SuppressFormsAuthenticationRedirect = true;
                    response.End();
                }
                else
                {
                    filterContext.Result =
                        new RedirectToRouteResult(
                        new RouteValueDictionary{{ "controller", "error" },
                                          { "action", "UnauthorizedAccess" }

                                         });

                    // base.HandleUnauthorizedRequest(filterContext);
                    //  filterContext.Result = new RedirectToRouteResult()
                }


            }

        }

    }

    public class IsReviewer : AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (DataModel.Utility.CurrentUser.CurrentUserDetail.UserName != null && (DataModel.Utility.CurrentUser.CurrentUserDetail.IsReviewPanelMember != true 
                && DataModel.Utility.CurrentUser.CurrentUserDetail.IsSuperAdmin !=true))
            {

                var httpContext = filterContext.HttpContext;
                var request = httpContext.Request;
                var response = httpContext.Response;
                if (request.IsAjaxRequest())
                {
                    response.StatusCode = 401;
                    response.SuppressFormsAuthenticationRedirect = true;
                    response.End();
                }
                else
                {
                    filterContext.Result =
                        new RedirectToRouteResult(
                        new RouteValueDictionary{{ "controller", "error" },
                                          { "action", "UnauthorizedAccess" }

                                         });

                    // base.HandleUnauthorizedRequest(filterContext);
                    //  filterContext.Result = new RedirectToRouteResult()
                }


            }

        }

    }
}