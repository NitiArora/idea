﻿using DataModel.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Web;

namespace FPIdeasWatch.Common
{
    public class Mail
    {

        private static bool SendMail(List<string> emailIds, string mailSubject, string body)
        {
            try
            {
                if (Convert.ToBoolean(DTIslandSetting.GetConfigValue("IsMailAlertOn")))
                {
                    var smtpClient = new SmtpClient(DTIslandSetting.GetConfigValue("MailServer"), Convert.ToInt32(DTIslandSetting.GetConfigValue("Port")));
                    smtpClient.Credentials = CredentialCache.DefaultNetworkCredentials;
                    var msg = new MailMessage();
                    msg.From = new MailAddress(DTIslandSetting.GetConfigValue("FromEmailId"), "Idea Next");
                    msg.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                    foreach (var emailId in emailIds)
                    {
                        msg.To.Add(emailId);

                    }

                    msg.Subject = mailSubject;
                    msg.IsBodyHtml = true;
                    msg.Body = body;

                    smtpClient.Send(msg);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
                // Logger.WriteLog("Exception in SendConfirmationEmail(string emailId, string url): " + ex);
            }



        }

        public static void SendAsyncEmail(List<string> emailIds, string mailSubject, string body)
        {

            Thread email = new Thread(delegate()
            {
                SendMail(emailIds, mailSubject, body);
            });

            email.IsBackground = true;
            email.Start();

        }
        public static void SendNotificationMail(DataModel.BO.IdeasWatchDetail ideaWatch, string subject1, List<string> emails,string modifiedByName)
        {
            var url = DataModel.Utility.DTIslandSetting.GetConfigValue("WebsiteUrl");
            var subject = subject1;
            var template = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/MailTemplate/IdeaNotification.html"));
            template = template.Replace("##subject##", subject);
            template = template.Replace("##IdeaCode##", "#" + ideaWatch.IdeaCode);
            template = template.Replace("##CurrentStatus##", ideaWatch.Status);
            template = template.Replace("##title##", ideaWatch.Title);
            template = template.Replace("##Createdby##", ideaWatch.CreatedByEmail);
            template = template.Replace("##modifiedby##", modifiedByName);
            template = template.Replace("##websiteurl##", url);
            var body = template;
            SendAsyncEmail(emails, subject, body);

            //List<string> emails = BusinessLogicLayer.BLL.User.GetEmailListOfReviewPanel();
            //var url = DataModel.Utility.DTIslandSetting.GetConfigValue("WebsiteUrl");
            //var subject = "New Ticket #" + sr.Result.IdeaCode + " created";
            //var template = System.IO.File.ReadAllText(System.Web.HttpContext.Current.Server.MapPath("~/MailTemplate/IdeaNotification.html"));
            //template = template.Replace("##subject##", subject);
            //template = template.Replace("##IdeaCode##", "#" + sr.Result.IdeaCode);
            //template = template.Replace("##CurrentStatus##", sr.Result.Status);
            //// template = template.Replace("##priority##", sr.Result.PriorityName);
            //template = template.Replace("##title##", sr.Result.Title);
            ////----- template = template.Replace("##raisedby##", sr.Result.);
            //template = template.Replace("##modifiedby##", CurrentUser.CurrentUserDetail.Email);
            //template = template.Replace("##websiteurl##", url);

            //var body = template;// "New ticket <b>#" + sr.Result + "</b> is created.<br/> Please <a href='" + url + "'> click here </a> to response";
            //SendAsyncEmail(emails, subject, body);
        }

    }
}