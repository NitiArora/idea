﻿using DataModel.Utility;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;




namespace FPIdeasWatch.Common
{
    public class Common
    {
        public static int GetTotalNumberOfIdeas()
        {
            return BusinessLogicLayer.BLL.IdeasWatch.GetTotalNumberOfIdeas();
        }
        public static string SeverPathOfUploadedFile()
        {
            return HttpContext.Current.Server.MapPath("~/App_Data/UploadedFile/");
          
        }

        public static List<Dictionary<string, string>> CreateMediaPath(List<HttpPostedFileBase> files)
        {
            List<Dictionary<string, string>> fileInfo = new List<Dictionary<string, string>>();
            try
            {
                //string pathValue = GetConfigValue("VirtualImagePath");
                string VirtualImagePath = DataModel.Utility.Base.GetConfigValue("VirtualImagePath");
                string PhysicalImagePath = DataModel.Utility.Base.GetConfigValue("PhysicalImagePath");
                string fileName = string.Empty;
                Dictionary<string, string> obj = new Dictionary<string, string>();
                foreach (var file in files)
                {
                    obj = new Dictionary<string, string>();
                    if (file != null)
                    {
                            if (!Directory.Exists(PhysicalImagePath))
                            {
                                DirectoryInfo di = Directory.CreateDirectory(PhysicalImagePath);

                            }
                            fileName = Path.GetFileName(DateTime.Now.Day + "_" + DateTime.Now.Month + "_" + "_" + DateTime.Now.Year + "_" + DateTime.Now.Millisecond + file.FileName);
                            file.SaveAs(Path.Combine(PhysicalImagePath, fileName));
                            obj["FileName"] = fileName;
                            obj["FilePath"] = VirtualImagePath;
                            obj["ContentType"] = file.ContentType;
                            obj["FileType"] = Path.GetExtension(file.FileName).ToLower();

                            fileInfo.Add(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.WriteLog("Exception in CreateMediaPath(List<HttpPostedFileBase> files): " + ex);
            }
            return fileInfo;

        }


      
    }

}