﻿bkLib.onDomLoaded(function () { nicEditors.allTextAreas() });
var storedFiles = [];
var upc = 0;
var scrol = 0;
var isNoScrol = false;
var _pageNumber = 1;
var _filterBy = 1;
var _ideaKeyId = "";
var _ideaKeyName = "";
$(document).ready(function () {
    isNoScrol = false;
    scrol = 0;
    $.ajaxSetup({
        statusCode: {
            401: function () {
                window.location.href = "/Error/SessionExpired";
            }
        }
    });

    $('#keys').tagsInput();
    var isEditIdea = $('#EditIdea').data('isedited');
    if (isEditIdea == 1)
    {
        swal(
 'Success!',
 'Idea Successfully Updated!',
 'success')
    }
   
    _ideaKeyName = $('#IdeaPage').data('keyname');
    _ideaKeyId = $('#IdeaPage').data('keyid');

    $('.disabledCalss').attr('disabled', true);

});
$(window).bind("load", function () {



    var isEditMode = parseInt($('#hdnEditMode_addIdea').data('isedit'));
    var char = 200;
    $("#counter").html(char);
    if (isEditMode == 1) {
        $("#counter").css("color", "green");
        $("#counter").html("Done");
        $("#counter-digit").html(0);
    }

    $("#Description").prev().on('keyup', function () {
        if ($(this).text().length > char) {
            //  $(this).text($(this).text().substr(1));           
        }

        var rest = char - $(this).text().length;
        $("#counter-digit").html(rest);
        if (rest <= 0) {
            $("#counter").css("color", "green");
            $("#counter").html("Done");
        }
        else {
            $("#counter").html(rest);
        }

    });

});
var currentPage = 1;


$(document).ajaxStart(function () {
    //  $('.alert').hide();
    // alert('hii');
    $('#loading, #app-loader-overlay').show();
});
$(document).ajaxComplete(function () {
    //  alert('bye');
    //  $('.alert').hide();
    $('#loading, #app-loader-overlay').hide();

});


$(document).on('click', '#btnSavePost', function () {
    var descriptionCount = parseInt($('#counter-digit').html());
    var category = parseInt($('#IdeaCategoryId').val());
    var title = $('#Title').val();
    var txtDescription = $('div#description-container').find('div.nicEdit-main').html();
    $('#Description').val(txtDescription);
    var locationId = $('#LocationId').val();
   // alert(locationId);
    var ReplicatedId = $('#ReplicatedId').val();
    var RelatedToId = $('#RelatedToId').val();
    var CurrentScenario = $('#CurrentScenario').val();
    var BenifitIdea = $('#BenifitIdea').val();
    if (category == 0)
    {
        callWarningMsg('Category should not be blank');
    }
    else if (title.length <= 0) {
        callWarningMsg('Title should not be blank');
    }
    else if (txtDescription.length <= 0 || txtDescription == "<br>")
    { callWarningMsg('Description should not be blank') }
    else if (descriptionCount > 0) {
        callWarningMsg('Min. 200 characters  are required in description')
    }
        else if (locationId!=undefined && locationId.length <= 0) {
        callWarningMsg('Location should not be blank');
    }
    else if (ReplicatedId.length <= 0) {
        callWarningMsg('Replicated should not be blank');
    }
    else if (RelatedToId.length <= 0) {
        callWarningMsg('RelatedTo should not be blank');
    }
    else {
        $('#frmID').submit();
    }



});
$(document).on("click", "#btnCancelEditIdea", function () {
    window.history.go(-1);
});
$(document).on('click', '.linkLikeIdea,.linkUnLikeIdea', function () {


    // alert('hii');
    scrol = $(document).scrollTop();
    isNoScrol = true;
    // $('#loading, #app-loader-overlay').show();
    var isLikedClick = false;
    var divId = $(this).parent('div.IdeaLikeUnlike-count');
    var ideaId = $(this).data('ideaid');
    var isLikedValue = $(this).data('isliked');
    var isLiked = false;
    if (isLikedValue == '1') {
        isLiked = true;
    }
    $.ajax({
        url: '/IdeaWatch/AddIdeaLikeDislike/',
        async: true,
        data: { IdeaWatchId: ideaId, isLiked: isLiked }
    }).done(function (data) {
        $(divId).html(data);

        $(".mypopover").popover();
        //  $('#loading, #app-loader-overlay').hide();
    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $(document).scroll(function () {
        if (isNoScrol == true) {
            $(document).scrollTop(scrol);
            isNoScrol = false;
        }
    });
});

$(document).on('click', '.btnPostCommnet', function () {
    scrol = $(document).scrollTop();
    isNoScrol = true;

    var divId = $(this).closest('div.commnetsContainer').find('div.commnetsContainer-list');
    var textAreaId = $(this).parent('div.textareaContainer').find('div.nicEdit-main');
    var commnetCountId = $(this).closest('div.commnetsContainer-main').find('span.commnetsContainer-main-count');
    var commentCount = parseInt($(commnetCountId).html());
    var txtComments = $(this).parent('div.textareaContainer').find('div.nicEdit-main').html();
    var ideaWatchId = $(this).data('ideawatchid');
    if (txtComments.length <= 0 || txtComments == "<br>")
    { callWarningMsg('Comment should not be blank') }
    else {
        $.ajax({
            url: '/IdeaWatch/AddComent/',
            async: true,
            type: 'Post',
            data: { txtComments: txtComments, ideaWatchId: ideaWatchId }
        }).done(function (data) {
            $(divId).html(data);
            $(textAreaId).html('');
            commentCount++;
            $(commnetCountId).html(commentCount);
            $(".mypopover").popover();

            $('.txtCommentsReply').keypress(function (e) {
                if (e.which == '13') {
                    $(this).parent('.textareaContainer').find('a.btnPostCommnetReply').click();
                }
            });

        }).fail(function () {
            callErrorMsg('There is some error please try again');
        });

        $(document).scroll(function () {
            if (isNoScrol == true) {
                $(document).scrollTop(scrol);
                isNoScrol = false;
            }
        });

    }



});

$(document).on('click', '.btnPostCommnetReply', function () {
    scrol = $(document).scrollTop();
    isNoScrol = true;
    var divId = $(this).closest('div.commnetReplyContainer').find('div.commnetReplyContainer-list');
    var textAreaId = $(this).parent('div.textareaContainer').find('input.txtCommentsReply');// $(this).parent('div.txtCommentsReply').find('div.nicEdit-main');
    var commentReplyCountId = $(this).closest('div.commnetReplyContainer-main').find('span.commnetReplyContainer-main-count');
    var commentReplyCount = parseInt($(commentReplyCountId).html());
    var txtCommentReply = $(this).parent('div.textareaContainer').find('input.txtCommentsReply').val();//$(this).parent('div.textareaContainer').find('div.nicEdit-main').html();
    var commentId = $(this).data('commentid');
    if (txtCommentReply.length <= 0 || txtCommentReply == "<br>")
    { callWarningMsg('Reply should not be blank') }
    else {
        $.ajax({
            url: '/IdeaWatch/AddCommentReply/',
            async: true,
            type: 'Post',
            data: { txtReply: txtCommentReply, commentId: commentId }
        }).done(function (data) {
            $(divId).html(data);
            $(textAreaId).val('');
            commentReplyCount++;
            $(commentReplyCountId).html(commentReplyCount);
            $(".mypopover").popover();
        }).fail(function () {
            callErrorMsg('There is some error please try again');
        });

        $(document).scroll(function () {
            if (isNoScrol == true) {
                $(document).scrollTop(scrol);
                isNoScrol = false;
            }
        });
    }

});
//--------------------Page --  ViewIdeasWatch -------------------------------------------------------------------------------------------------------------

//function NotToScrol(scrol)
//{
//    $(document).scroll(function () {
//        if (isNoScrol == true) {
//            $(document).scrollTop(scrol);
//            isNoScrol = false;
//        }
//    });
//}


$(document).on('click', '.btnChangeStatus', function () {

    var isDetail = $('#pageDetail').data('isdetail');
    var ideaId = $(this).data('ideaid');
    var statusId = $(this).data('statusid');
    var cntl = $(this).closest('div.ideaStatus-container');
    var isNoScrol = true;
    $.ajax({
        url: '/IdeaWatch/ChangeIdeaStatus/',
        async: true,
        data: { ideaWatchId: ideaId, statusId: statusId }
    }).done(function (data) {

        if (data.IsSuccessful == true) {

            if (isDetail != undefined && isDetail == 1) {
                window.location.reload(true);
                
            }
            else {
                if (parseInt(statusId) == 3) {
                    $(cntl).html('<div class="idea-status is--approved btn-success"><i class="icon icon-checkmark-circle"></i>Approved</div>');
                }
                else if (parseInt(statusId) == 1) {
                    $(cntl).html('<div class="idea-status is--pending btn-warning"><i class="icon icon-time"></i>Pending</div>');
                }
                else if (parseInt(statusId) == 4) {
                    $(cntl).html('<div class="idea-status is--rejected btn-danger"><i class="icon icon-warning"></i>Rejected</div>');
                }
                else if (parseInt(statusId) == 2) {
                    $(cntl).html('<div class="idea-status is--progress btn-info"><i class="icon icon-meter"></i>InProgress</div>');
                }
                
            }
           

        }
        else {
            alert(data.Message);
        }

    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $(document).scroll(function () {
        if (isNoScrol == true) {
            $(document).scrollTop(scrol);
            isNoScrol = false;
        }
    });


    return false;

});



$(document).on('click', '.btnReplyCommnetDelete', function () {
    var replyCommentId = $(this).data('replycommentid');
    var cntl = $(this).closest('div.rply-comment-list');
    var commentReplyCountId = $(this).closest('div.commnetReplyContainer-main').find('span.commnetReplyContainer-main-count');
    var commentReplyCount = parseInt($(commentReplyCountId).html());
    var isNoScrol = true;
    $.ajax({
        type: "Post",
        url: '/IdeaWatch/DeleteCommentReply/',
        data: { replyCommentId: replyCommentId }
    }).done(function (data) {

        if (data == true) {
            $(cntl).hide();
            commentReplyCount--;
            $(commentReplyCountId).html(commentReplyCount);
        }

    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $(document).scroll(function () {
        if (isNoScrol == true) {
            $(document).scrollTop(scrol);
            isNoScrol = false;
        }
    });


    return false;

});


$(document).on('click', '.btnPostCommnetDelete', function () {
    var commentid = $(this).data('commentid');
    var cntl = $(this).closest('div.comment-list');
    var isNoScrol = true;
    $.ajax({
        type: "Post",
        url: '/IdeaWatch/DeleteComment/',
        async: true,
        data: { commentId: commentid }
    }).done(function (data) {

        if (data == true) {
            $(cntl).hide();
        }

    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $(document).scroll(function () {
        if (isNoScrol == true) {
            $(document).scrollTop(scrol);
            isNoScrol = false;
        }
    });


    return false;

});




function GetFilteredIdeasView(page) {
    scrol = $(document).scrollTop();
    isNoScrol = true;
    //var keys = "";
    //$('#listIdeaKey option:selected').each(function () {
    //    keys += $(this).val() + ",";
    //});
    var ideaStatusId = $('.ideaStatus').data('status');
    currentPage = page;
    $.ajax({
        url: '/IdeaWatch/GetFilteredIdeaWatchViewList/',
        async: true,
        data: { page: currentPage, filterBy: _filterBy, keys: _ideaKeyId, ideaStatusId: ideaStatusId }
    }).done(function (data) {
        $('#ideaListView-container').html(data);
        RemoveHrefOfpaging();
        AddNicEditerOnDynamicTextArea();
        $(".mypopover").popover();

        if (parseInt(_filterBy) == 1) {
            $('#lblIdea-sortBy').html("Recently Added ");
        }
        if (parseInt(_filterBy) == 2) {
            $('#lblIdea-sortBy').html("Most Liked ");
        }
        if (parseInt(_filterBy) == 4) {
            $('#lblIdea-sortBy').html("Most commneted  ");
        }

        if (parseInt(ideaStatusId) == 2) {
            $('#lblIdea-sortBy').html("Pending Review");
        }

        $('#lblIdea').html(_ideaKeyName);


    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $(document).scroll(function () {
        if (isNoScrol == true) {
            $(document).scrollTop(scrol);
            isNoScrol = false;
        }
    });


    return false;
}


function AddNicEditerOnDynamicTextArea() {
    $('#addScnt').click();
}

$(function () {
    var editors = {};
    var scntDiv = $('#p_scents');
    $(document).on('click', '#addScnt', function () {
        $('textarea.txtComments').each(function () {
            var elm = $(this);// $('<textarea NAME="description[]"></textarea>').appendTo(this); // Add the textarea to DOM
            var curSize = $(this).length; //Get the current SIZE of textArea
            editors[curSize] = new nicEditor().panelInstance(elm[0]); //Set the Object with t
        });
    });

});


$(document).on('click', '.btnkey_idea', function () {
    _ideaKeyId = $(this).data('keyid');
    _ideaKeyName = "'" + $(this).text() + "' Ideas";
    GetFilteredIdeasView(1);
});

$(document).on('click', '.btnDeleteIdea', function () {
    var parentDiv = $(this).parent().parent('.ideanote-block');
    var ideaWatchId = $(this).data('ideaid');
    if (confirm("Are you sure you want to delete this?")) {
        DeleteIdeaWatch(ideaWatchId, _pageNumber, parentDiv);
    };
    return false;
});

$(document).on('click', '#saveNewCategoryname', function () {
    var txtCategoryName = $("#txtCategoryName").val();
    if (txtCategoryName =="")
    {
        callErrorMsg('Category should not be blank');
    }
    else
    {
        var CategoryModel =
                {
                    categoryName: txtCategoryName
                };
        $.ajax({
            url: '/IdeaWatch/AddNewCategory/',
            data: JSON.stringify(CategoryModel),
            type: 'POST',
            contentType: 'application/json; charset=utf-8'
        }).done(function (data) {
            callSuccessMsg('Your request successfully processed');
            window.location.reload(true);
        }).fail(function () {
            callErrorMsg('There is some error please try again');
        });
    }
});

$(document).on('click', '.btnCategoryDelete', function () {
    var CategoryId = $(this).data('categoryid');
    var CategoryName = $(this).data('name');
    var CategoryModel =
                {
                    categoryId: CategoryId
                };
        $.ajax({
            url: '/IdeaWatch/RemoveCategory/',
            data: JSON.stringify(CategoryModel),
            type: 'POST',
            contentType: 'application/json; charset=utf-8'
        }).done(function (data) {
            if (data == true) {
                window.location.reload(true);
            }
            else {
                callErrorMsg('There is some error please try again');
            }
        }).fail(function () {
            callErrorMsg('There is some error please try again');
        });
});



function DeleteIdeaWatch(ideaWatchId, page,parentDiv) {
    scrol = $(document).scrollTop();
    isNoScrol = true;
    var ideaPage = parseInt($('#IdeaPage').val());
   

    $.ajax({
        url: '/IdeaWatch/DeleteIdea/',

        data: { ideaWatchId: ideaWatchId, page: page }
    }).done(function (data) {
        //GetFilteredIdeasView(1)
        //if (ideaPage == 1)
        //{
        //    $('#Ideas').html(data);
        //}
        //else if (ideaPage == 2) {
        //    $('#ViewIdeasContainer').html(data);
        //}
        //alert(data);
        $(parentDiv).fadeOut();
       // window.location.reload(true);
        //$('#ViewIdeasContainer').html(data);

        //RemoveHrefOfpaging();
        //AddNicEditerOnDynamicTextArea();
        //$(".mypopover").popover();
    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $(document).scroll(function () {
        if (isNoScrol == true) {
            $(document).scrollTop(scrol);
            isNoScrol = false;
        }
    });
}

$(function () {
    $(document).ready(function () {
        //$('[data-toggle="popover"]').popover();
    });
    $('.ideanote_list li:last-child').addClass('last');

    $('.noteTitle').hover(function () {
        $('.tip').css({ 'marginTop': -$(this).parent().parent().parent().parent().find('.tip').height() / 2 + 'px' })
    });
});
//$(document).on('.txtCommentsReply', 'keypress', function (e) {
$('.txtCommentsReply').keypress(function (e) {
    if (e.which == '13') {
        $(this).parent('.textareaContainer').find('a.btnPostCommnetReply').click();
    }
});
