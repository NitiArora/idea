﻿function callErrorMsg(response) {
    $('.alert').hide();
    var content = "<h4><i class=\"icon icon-ban-circle\"></i> <strong>Error!</strong></h4><p>" + response + "</p>";
    $('#mainErrorMsg1').html(content);
    $('#mainErrorMsg').show();
}
function callSuccessMsg(response) {
    $('.alert').hide();
    var content = "<h4><i class=\"icon icon-ok-sign\"></i> <strong>Well done!</strong></h4><p>" + response + "</p>";
    $('#mainSuccessMsg1').html(content).show();
    $('#mainSuccessMsg').show();
}
function callWarningMsg(response) {
    $('.alert').hide();
    var content = "<h4><i class=\"icon icon-bell-alt\"></i> <strong>Warning!</strong></h4><p>" + response + "</p>";
    $('#mainWarningMsg1').html(content).show();
    $('#mainWarningMsg').show();
}