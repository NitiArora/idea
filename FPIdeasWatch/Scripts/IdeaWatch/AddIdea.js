﻿var scrol = 0;
var isNoScrol = false;
var _pageNumber = 1;
var _filterBy = 1;
var _ideaKeyId = "";
var _ideaKeyName = "";
var _ideaStatusId = 1;
$(document).ready(function () {
    //$('#lblIdea-sortBy').html("Pending Review");
    //$('#ddlIdeaStatusList').val(_ideaStatusId);
    var isadded = $('#addIdea').data('isadded');
    var addIdeaCode = $('#addIdeaCode').data('ideaaddedcode');
    if (isadded == 1) {
   swal(
  'Success!',
  'Idea ' + addIdeaCode + ' Successfully Added!',
  'success')
    }
    RemoveHrefOfpaging_AddIdea();
    // $('[data-toggle="popover"]').popover();
});

function RemoveHrefOfpaging_AddIdea() {
    $('.pagedList').find('a').each(function () {
        var href = String($(this).attr('href'));
        var numberSlip = href.split("=");
        var pageNumber = numberSlip[1];
        $(this).attr('href', '#');
        $(this).attr('rel', pageNumber);
        $(this).attr('class', 'btnPagingAddIdea');
    });
}


$(document).on("click", ".btnPagingAddIdea", function () {
    var rel = $(this).attr('rel');
    _pageNumber = 1;
    if (rel != undefined) {
        _pageNumber = rel;
    }

    GetIdeaListForAddIdea(_pageNumber);
});

function GetIdeaListForAddIdea(page) {
    scrol = $(document).scrollTop();
    isNoScrol = true;
   
    currentPage = page;
    $.ajax({
        url: '/IdeaWatch/GetIdeaListForAddIdeaOnPaging/',
        async: true,
        data: { page: currentPage}
    }).done(function (data) {
        $('#ideaListView-container').html(data);
        RemoveHrefOfpaging_AddIdea();
        AddNicEditerOnDynamicTextArea();
    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $(document).scroll(function () {
        if (isNoScrol == true) {
            $(document).scrollTop(scrol);
            isNoScrol = false;
        }
    });


    return false;
}