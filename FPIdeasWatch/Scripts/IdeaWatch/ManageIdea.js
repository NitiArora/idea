﻿var storedFiles = [];
var upc = 0;
var scrol = 0;
var isNoScrol = false;
var _pageNumber = 1;
var _filterBy = 1;
var _ideaKeyId = "";
var _ideaKeyName = "";
var _ideaStatusId = 1;
//--------------------Page --  PendingReview ------------------------------------------------------
$(document).ready(function () {
    //$('#lblIdea-sortBy').html("Pending Review");
    //$('#ddlIdeaStatusList').val(_ideaStatusId);
    RemoveHrefOfpaging();
   // $('[data-toggle="popover"]').popover();
});



//$('.btnPaging').click(function () {
//    var rel = $(this).attr('rel');
//    _pageNumber = 1;
//    if (rel != undefined) {
//        _pageNumber = rel;
//    }

//    GetFilteredIdeasView(_pageNumber);
//});


function GetFilteredIdeasView(page) {
    scrol = $(document).scrollTop();
    isNoScrol = true;
    //var keys = "";
    //$('#listIdeaKey option:selected').each(function () {
    //    keys += $(this).val() + ",";
    //});
    //var ideaStatusId = $('.ideaStatus').data('status');
    var ideaStatusId = _ideaStatusId;
    var categoryId = $('#ddlCategory_View').val();
    alert(categoryId);
    currentPage = page;
    $.ajax({
        url: '/IdeaWatch/GetFilteredIdeaWatchViewList/',
        async: true,
        data: { page: currentPage, filterBy: _filterBy, keys: _ideaKeyId, ideaStatusId: ideaStatusId, categoryId: categoryId }
    }).done(function (data) {
        $('#ideaListView-container').html(data);
        RemoveHrefOfpaging();
        AddNicEditerOnDynamicTextArea();
        $(".mypopover").popover();

        if (parseInt(_filterBy) == 1) {
            $('#lblIdea-sortBy').html("Recently Added ");
        }
        if (parseInt(_filterBy) == 2) {
            $('#lblIdea-sortBy').html("Most Liked ");
        }
        if (parseInt(_filterBy) == 4) {
            $('#lblIdea-sortBy').html("Most commneted  ");
        }

        if (parseInt(ideaStatusId) == 2) {
            $('#lblIdea-sortBy').html("Pending Review");
        }

        $('#lblIdea').html(_ideaKeyName);


    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $(document).scroll(function () {
        if (isNoScrol == true) {
            $(document).scrollTop(scrol);
            isNoScrol = false;
        }
    });


    return false;
}
function RemoveHrefOfpaging() {
    $('.pagedList').find('a').each(function () {
        var href = String($(this).attr('href'));
        var numberSlip = href.split("=");
        var pageNumber = numberSlip[1];
        $(this).attr('href', '#');
        $(this).attr('rel', pageNumber);
        $(this).attr('class', 'btnPagingManageIdea');
    });
}

$("#ddlIdeaStatusList").change(function () {
    $('#lblIdea-sortBy').html(" ");
    _ideaStatusId = $('#ddlIdeaStatusList').val();
    var IdeaStatusModel =
                    {
                        ideaStatusid: _ideaStatusId
                    };
    $.ajax({
        url: '/IdeaWatch/GetIdeaByStatusForManageIdea/',
        cache: false,
        data: JSON.stringify(IdeaStatusModel),
        type: 'POST',
        contentType: 'application/json; charset=utf-8'
    }).done(function (data) {
        $('#ViewIdeasContainer-pending').html(data);
        RemoveHrefOfpaging();
    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

});


$(document).on("click", ".btnPagingManageIdea", function () {
    var rel = $(this).attr('rel');
    _pageNumber = 1;
    if (rel != undefined) {
        _pageNumber = rel;
    }

    GetIdeaListForManageIdea(_pageNumber);
});

function GetIdeaListForManageIdea(page) {
    scrol = $(document).scrollTop();
    isNoScrol = true;
    //var keys = "";
    //$('#listIdeaKey option:selected').each(function () {
    //    keys += $(this).val() + ",";
    //});
    var ideaStatusId = $('#ddlIdeaStatusList').val();
    currentPage = page;
    $.ajax({
        url: '/IdeaWatch/GetIdeaListForManageIdeaOnPaging/',
        async: true,
        data: { page: currentPage, ideaStatusId: ideaStatusId }
    }).done(function (data) {
        $('#ideaListView-container').html(data);
        RemoveHrefOfpaging();
        AddNicEditerOnDynamicTextArea();
        $(".mypopover").popover();
        $('#lblIdea-sortBy').html("Recently Added ");
        //if (parseInt(_filterBy) == 1) {
        //    $('#lblIdea-sortBy').html("Recently Added ");
        //}
        //if (parseInt(_filterBy) == 2) {
        //    $('#lblIdea-sortBy').html("Most Liked ");
        //}
        //if (parseInt(_filterBy) == 4) {
        //    $('#lblIdea-sortBy').html("Most commneted  ");
        //}

        //if (parseInt(ideaStatusId) == 2) {
        //    $('#lblIdea-sortBy').html("Pending Review");
        //}

        $('#lblIdea').html(_ideaKeyName);


    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $(document).scroll(function () {
        if (isNoScrol == true) {
            $(document).scrollTop(scrol);
            isNoScrol = false;
        }
    });


    return false;
}


