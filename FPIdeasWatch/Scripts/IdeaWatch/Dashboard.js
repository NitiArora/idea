﻿$(document).ready(function () {
    
   
    GetDataForIdeaStatus();
    

    //$(function () {

    //    $('#container2').highcharts({
    //        chart: {
    //            plotBackgroundColor: null,
    //            plotBorderWidth: null,
    //            plotShadow: false,
    //            type: 'pie'
    //        },
    //        title: {
    //            text: 'Approved Ideas Status'
    //        },
    //        tooltip: {
    //            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
    //        },
    //        plotOptions: {
    //            pie: {
    //                allowPointSelect: true,
    //                cursor: 'pointer',
    //                dataLabels: {
    //                    enabled: true,
    //                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
    //                    style: {
    //                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
    //                    }
    //                }
    //            }
    //        },
    //        series: [{
    //            name: 'Brands',
    //            colorByPoint: true,
    //            data: [{
    //                name: 'Closed ',
    //                y: 40,
    //                sliced: true,
    //                selected: true
    //            }, {
    //                name: 'In Progress',
    //                y: 60

    //            }]
    //        }]
    //    });
    //});




});

function GetDataForIdeaStatus()
{
   
    $.ajax({
        url: '/Dashboard/GetIdeaStatusDataForChartData/',
        type: 'Get',
        contentType: 'application/json; charset=utf-8',
        data: {  }
    }).done(function (Result) {
       // Result = Result.d;
        var data = [];
        var liList = "";
        for (var i in Result) {
            if (Result[i].Id == 1) {
                liList += '<li class="btnIdeaStatus_dashboard btn-warning col-md-3  liFilter_' + Result[i].Id + '"  data-ideastatusid="' + Result[i].Id + '"><i class="icon icon-time"></i><span>' + Result[i].Name + ' :</span> <span>' + Result[i].Value + '</span></li>';
            }
            else if (Result[i].Id == 2) {
                liList += '<li class="btnIdeaStatus_dashboard btn-info col-md-3  liFilter_' + Result[i].Id + '"  data-ideastatusid="' + Result[i].Id + '"><i class="icon icon-meter"></i><span>' + Result[i].Name + ' :</span> <span>' + Result[i].Value + '</span></li>';
            }
            else if (Result[i].Id == 3) {
                liList += '<li class="btnIdeaStatus_dashboard btn-success col-md-3 liFilter_' + Result[i].Id + '"  data-ideastatusid="' + Result[i].Id + '"><i class="icon icon-checkmark-circle"></i><span>' + Result[i].Name + ' :</span> <span>' + Result[i].Value + '</span></li>';
            }
            else if (Result[i].Id == 4) {
                liList += '<li class="btnIdeaStatus_dashboard btn-danger col-md-3 liFilter_' + Result[i].Id + '"  data-ideastatusid="' + Result[i].Id + '"><i class="icon icon-warning"></i><span>' + Result[i].Name + ' :</span> <span>' + Result[i].Value + '</span></li>';
            }
            else if (Result[i].Id == 5) {
                liList += '<li class="btnIdeaStatus_dashboard btn-inprogress col-md-3 liFilter_' + Result[i].Id + '"  data-ideastatusid="' + Result[i].Id + '"><i class="icon icon-time"></i><span>' + Result[i].Name + ' :</span> <span>' + Result[i].Value + '</span></li>';
            }
            else if (Result[i].Id == 6) {
                liList += '<li class="btnIdeaStatus_dashboard btn-done col-md-3 liFilter_' + Result[i].Id + '"  data-ideastatusid="' + Result[i].Id + '"><i class="icon icon-checkmark-circle"></i><span>' + Result[i].Name + ' :</span> <span>' + Result[i].Value + '</span></li>';
            }
            else if (Result[i].Id == 7) {
                liList += '<li class="btnIdeaStatus_dashboard btn-live col-md-3 liFilter_' + Result[i].Id + '"  data-ideastatusid="' + Result[i].Id + '"><i class="icon icon-checkmark-circle"></i><span>' + Result[i].Name + ' :</span> <span>' + Result[i].Value + '</span></li>';
            }
          
            var serie = new Array(Result[i].Name, Result[i].Value,Result[i].Id);
            data.push(serie);
        }
        var aa = window.location.href;
        $('#chartDataDetail').html(liList);
        var bb = aa.split('=');
       
        if (bb[1] != undefined) {
            $("li.liFilter_" + bb[1] + "").addClass('active');
        }
        else {
            $("li.liFilter_3").addClass('active');
        }
        
       
        //$('#chartDataDetail ul li> ul li:eq(2)').css('border', '1px solid red');
       // $("ul  li:nth-child(3)").addClass('active');
       // var myJSONText = JSON.stringify(data);
        BindChartForIdeaStatus(data);
    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

}

function BindChartForIdeaStatus(series,title) {
    $('#container1').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        colors: ['#ED9C28', '#5BC0DE', '#5CB85C', '#D2322D', '#2348CB', '#11B1B4', '#AA3FC4'],
        title: {
            text: ""
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                },
                point: {
                    events: {
                        click: function (event) {
                            $('.btnIdeaStatus_dashboard').removeClass('active');
                            var options = this.options;
                            if(options.name =="Pending")
                            {
                                GetIdeaWithStatus(1);// 1 --> Pending
                                $('.liFilter_1').addClass('active');
                            }
                            if(options.name =="Review")
                            {
                                GetIdeaWithStatus(2);// 2 --> Review
                                $('.liFilter_2').addClass('active');
                            }
                            if(options.name =="Approved") {
                                GetIdeaWithStatus(3);// 3 ---> Approved
                                $('.liFilter_3').addClass('active');
                            }
                            if(options.name =="Rejected") {
                                GetIdeaWithStatus(4);// 4 ----> Rejected
                                $('.liFilter_4').addClass('active');

                            }
                            if (options.name == "InProgress") {
                                GetIdeaWithStatus(5);// 5 ----> InProgress
                                $('.liFilter_5').addClass('active');

                            }
                            if (options.name == "Done") {
                                GetIdeaWithStatus(6);// 6 ----> Done
                                $('.liFilter_6').addClass('active');

                            }
                            if (options.name == "Live") {
                                GetIdeaWithStatus(7);// 7 ----> Live
                                $('.liFilter_7').addClass('active');

                            }

                        }
                    }
                },
                showInLegend: true
            }
        },
        series: [{
            type: 'pie',
            name: 'Ideas',
           // colorByPoint: true,
            data: series
            
        }]
    });
}


$(document).on('click', '.btnIdeaStatus_dashboard', function () {
    $('.btnIdeaStatus_dashboard').removeClass('active');
    $(this).addClass('active');
    var ideastatusid = $(this).data('ideastatusid');
    if (window.location.href.indexOf("ideaStatusId") > -1) {
        $.ajax({
            url: '/Dashboard/GetIdeaWithStatusForIdeaList/',
            data: { ideaStatusid: ideastatusid },
            type: 'Get',
            contentType: 'application/json; charset=utf-8'
        }).done(function (data) {
            $('#ideaList_Dashboard').html(data);
            $('#btnMoreIdea_Dashboard').hide();
            $('#spnTop10').hide();
        }).fail(function () {
            callErrorMsg('There is some error please try again');
        });
    }
    else {
        GetIdeaWithStatus(ideastatusid);
    }
   
});

//$(document).on('click', '.btn_data', function () {
//    var ideastatusid = $(this).data('ideavalueid');
//    $.ajax({
//        url: '/Dashboard/IdeaList/',
//        data: { ideaStatusid: ideastatusid },
//        type: 'Get',
//        contentType: 'application/json; charset=utf-8'
//    }).done(function (data) {
//        $('#ideaList_Dashboard').html(data);
//    }).fail(function () {
//        callErrorMsg('There is some error please try again');
//    });
//   // window.location.href = '/Dashboard/IdeaList?a=0';
//});


function GetIdeaWithStatus(ideastatusid) {
    $.ajax({
        url: '/Dashboard/GetIdeaWithStatusForDashboard/',
        data: { ideaStatusid: ideastatusid },
        type: 'Get',
        contentType: 'application/json; charset=utf-8'
    }).done(function (data) {
        $('#ideaList_Dashboard').html(data);
        $('#spnTop10').show();
        $('#btnMoreIdea_Dashboard').show();
    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });
}
