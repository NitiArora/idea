﻿$('#saveNewMember').on('click', function () {
    var ids = [];
    $('#pageNameOwnerLi .multiText li').each(function (i, j) {
        ids.push(parseInt($(this).data('id')));
    });
    var ReviewPanelModel =
                {
                    userId: ids
                };
    if (ids.length > 0) {
        $.ajax({
            url: '/ManageUser/AddingMembersInReviewPanel/',
            data: JSON.stringify(ReviewPanelModel),
            type: 'POST',
            contentType: 'application/json; charset=utf-8'
        }).done(function (data) {
            if (data == true) {
                callSuccessMsg('Your request successfully processed');
                //setTimeout(function () {
                //    $('#mainSuccessMsg').hide();
                    window.location.reload(true);
                 //}, 5000);
               
            }
            else {
                callErrorMsg('There is some error please try again');
            }
        }).fail(function () {
            callErrorMsg('There is some error please try again');
        });
    }
    else {
        callErrorMsg('Review Panel Member can not be blank.');
    }
});

$('.btnpanelMemberDelete').on('click', function () {
    var userdetail = $(this).closest('li.list__item');
    var UserId = $(this).data('userid');
    var UserName = $(this).data('name');
    var ReviewPanelModel =
                {
                    userId: UserId
                };
    if (UserId > 0) {
        $.ajax({
            url: '/ManageUser/RemoveMemberFromReviewPanel/',
            data: JSON.stringify(ReviewPanelModel),
            type: 'POST',
            contentType: 'application/json; charset=utf-8'
        }).done(function (data) {
            if (data == true) {
                $(userdetail).fadeOut();
                window.location.reload(true);
                $('#itemListUl-PageName-container').append('<li id="' + UserId + '">' + UserName + '</li>');
            }
            else {
                callErrorMsg('There is some error please try again');
            }
        }).fail(function () {
            callErrorMsg('There is some error please try again');
        });
    }
    else {
        callErrorMsg('Review Panel Member can not be blank.');
    }
});