﻿$(document).ready(function () {
    fpMultiSelect();
});

function fpMultiSelect() {
    $('body').on('click', function () { clearList(); });
    $('.multiHolder').on('click', function () { return false; });
    $('.itemList li').each(function () { $(this).html($(this).text().toLowerCase()); });
    $('.multiText:not(.hold)').on('click', function () { $('.multiText input').not(this).val(''); $(this).find('input').focus(); clearList(); });

    $('.multiText input').on('keyup keydown', function () {
        mh = $(this).parent().parent();
        $(this).after('<span>' + ($(this).val()) + '</span>');
        cWidth = $(this).parent().find('span').width();
        $(this).parent().find('span').remove();
        $(this).width(cWidth + 10);


        $('.itemList ,.itemList li').addClass('hide').removeClass('show');
        cv = $(this).val().toLowerCase();
        $('.notification').remove();
        if (cv != '') {
            $(mh).find('.multiText ul li').removeClass('selected');
            $(mh).find('.itemList li:contains(' + cv + ')').addClass('show').removeClass('hide');
            $(mh).find('.multiText ul li').each(function () {
                selectedItemID = $(this).attr('id').substring(1, ($(this).attr('id').length));
                //$('#'+selectedItemID).addClass('hide').removeClass('show');
                $('.itemList li').each(function (index, element) {
                    if ((selectedItemID == $(this).attr('id'))) { $(this).addClass('hide').removeClass('show'); }
                }); // each sub
            }); //each main

            if ($(mh).find('li.show').size() < 1) {
                $(mh).find('.itemList').removeClass('show').addClass('hide');
                $(mh).find('.itemList').before('<div class="notification">Nothing Found!</div>');
            }
            else { $(mh).find('.itemList').addClass('show').removeClass('hide'); }
        } else {
            $(mh).find('.itemList').addClass('hide').removeClass('show');
            $(this).width(1);
        }
    });

    $('.multiText input').on('keydown', function (key) {
        if ($(this).parent().hasClass('hold')) { return false }
        keyNo = key.which;
        sc = $(mh).find('.itemList li.selected').size();
        switch (keyNo) {
            case 40:
                selectdown();
                setScroll();
                break;
            case 38:
                selectUp();
                setScroll();
                break;
            case 39:
                selectNext();
                break;
            case 37:
                selectPrev();
                break;
            case 46:
                deleteIt()
                break;
            case 13:
                selectItem();
                break;
            case 8:
                backSpace();
                break;
            default:
                $(mh).find('.itemList li.selected').removeClass('selected');
                if ($(this).hasClass('single') && ($(this).parent().find('li').size() >= 1 && keyNo != 9)) { return false; }
        }
    });

    function selectdown() {
        if (sc == 0) {
            $(mh).find('.itemList li.show:first').addClass('selected');
        } else {
            $(mh).find('.itemList li.show').each(function (index) {
                if ($(this).hasClass('selected')) {
                    cindex = index;
                }
                if (index == cindex + 1) {
                    $('.itemList li.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });
        }
    };

    function selectUp() {
        if (sc == 0) {
            $(mh).find('.itemList li.show:last').addClass('selected');
        } else {
            $(mh).find('.itemList li.show').each(function (index) {
                if ($(this).hasClass('selected')) {
                    cindex = index;
                }
            });
            $(mh).find('.itemList li.show').each(function (index) {
                if (index == cindex - 1) {
                    $(mh).find('.itemList li.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });
        }
    };

    function selectItem() {
        if (sc != 0) {
            selText = $(mh).find('.itemList li.selected').text();
            selId = $(mh).find('.itemList li.selected').attr('id');
            $(mh).find('.multiText ul').append('<li id="_' + selId + '" data-id="' + selId + '">' + selText + '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></li>');
            clearList();
        }
    };

    function backSpace() {
        $(mh).find('.multiText ul li.selected').remove();
        if ($(mh).find('.multiText input').val() == '' && $(mh).find('.multiText ul li').size() > 0) {
            $(mh).find('.multiText ul li:last').addClass('selected');
        }
    };

    function selectPrev() {
        if ($(mh).find('.multiText input').val() == '' && $(mh).find('.multiText ul li.selected').size() > 0) {
            $(mh).find('.multiText ul li.selected').removeClass('selected').prev().addClass('selected');
        }
        else if ($(mh).find('.multiText input').val() == '' && $(mh).find('.multiText ul li.selected').size() == 0) {
            $(mh).find('.multiText ul li:last').addClass('selected');
        }
    };

    function selectNext() {
        if ($(mh).find('.multiText input').val() == '' && $(mh).find('.multiText ul li.selected').size() > 0) {
            $(mh).find('.multiText ul li.selected').removeClass('selected').next().addClass('selected');
        }
        else if ($(mh).find('.multiText input').val() == '' && $(mh).find('.multiText ul li.selected').size() == 0) {
            $(mh).find('.multiText ul li:first').addClass('selected');
        }
    };

    function deleteIt() {
        if ($(mh).find('.multiText input').val() == '' && $(mh).find('.multiText ul li.selected').size() > 0) {
            $(mh).find('.multiText ul li.selected').next().addClass('dltIt');
            $(mh).find('.multiText ul li.selected').remove();
            $(mh).find('.multiText ul li.dltIt').toggleClass('dltIt selected');
        }
    };

    function clearList() { $('.itemList ,.itemList li').addClass('hide').removeClass('show').removeClass('selected'); $('.multiText input').val(''); $('.notification').remove(); };

    function setScroll() {
        scrolltop = $(mh).find('.itemList ul').scrollTop();
        listheight = $(mh).find('.itemList ul').height();
        activeTop = $(mh).find('.itemList li.selected').position().top;
        cScroll = $(mh).find('.itemList ul').scrollTop();

        if (activeTop >= listheight) {
            $(mh).find('.itemList ul').scrollTop((cScroll + activeTop) - listheight + 25);
        }
        else if (activeTop < 0) {
            $(mh).find('.itemList ul').scrollTop(cScroll + activeTop);
        }
    };

    $('.itemList ul').on('click scroll', function () {
        $(mh).find('input').focus();
    });

    $('.itemList li').on('mouseenter', function () {
        $('.itemList li').removeClass('selected');
        $(this).addClass('selected');
        sc = 1;
    });
    $('.itemList li').on('mouseleave', function () {
        $('.itemList li').removeClass('selected');
        sc = 0;
    });
    $('.itemList ul li').on('click', function () {
        selectItem();
    });
    $('.multiText').on('click', ".close", function () {
        if ($(this).parents('.multiText').hasClass('hold') || $(this).parents('.multiText input').is(':disabled')) { return false }
        $(this).parent().remove();
    });
};
