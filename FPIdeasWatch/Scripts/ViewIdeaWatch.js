﻿var upc = 0;
var scrol = 0;
var isNoScrol = false;
var _pageNumber = 1;
var _filterBy = 1;
var _ideaKeyId = "";
var _ideaKeyName = "";
var _ideaStatusId = 1;

$(document).ready(function () {
    isNoScrol = false;
    scrol = 0;
    $.ajaxSetup({
        statusCode: {
            401: function () {
                window.location.href = "/Error/SessionExpired";
            }
        }
    });
    RemoveHrefOfpaging();
   GetCloudKey();
});

//------------------------------------------

$(document).on('click', '.btnFilterIdea', function () {
    var filterById = $(this).data('fiterid');
    if (filterById == 1)
    {
        $('#viewidea_link1').addClass('active');
        $('#viewidea_link2').removeClass('active');
        $('#viewidea_link4').removeClass('active');
    }
    else if (filterById == 2) {
        $('#viewidea_link2').addClass('active');
        $('#viewidea_link1').removeClass('active');
        $('#viewidea_link4').removeClass('active');
    }
   else if (filterById == 4) {
       $('#viewidea_link4').addClass('active');
       $('#viewidea_link1').removeClass('active');
       $('#viewidea_link2').removeClass('active');
    }


    _filterBy = filterById;
    _ideaKeyId = "";
    _ideaKeyName = "";
    GetFilteredIdeasView_ViewIdea(1);
});
$(document).on("click", ".btnPaging", function () {
    var rel = $(this).attr('rel');
    _pageNumber = 1;
    if (rel != undefined) {
        _pageNumber = rel;
    }

    GetFilteredIdeasView_ViewIdea(_pageNumber);
});

function GetCloudKey() {
    $.ajax({
        url: '/IdeaWatch/GetKeyCloudList/',
        async: true,
        data: {}
    }).done(function (data) {
        var json = JSON.parse(data);

        $('#keyController-main').jQCloud(json, {
            width: 400,
            height: 300,
            afterCloudRender: function () {
                $('#keyController-main  a').each(function () {
                    $(this).attr('class', 'btnkeyCloud');
                });
                $(".btnkeyCloud").bind("click", function () {
                    var href = $(this).attr('href');
                    var hrefSplit = href.split('#');
                    _ideaKeyId = hrefSplit[1];
                    var text = "'" + $(this).text() + "' Ideas";
                    _ideaKeyName = text;
                    GetFilteredIdeasView_ViewIdea(1);
                   // $('#lblIdea').html(text);
                });
            },

        });


    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $('#keyController-main  span').each(function () {
        var aa = $(this).attr('id');
        $(this).attr('class', 'keyCloud');
    });
}
function RemoveHrefOfpaging() {
    $('.pagedList').find('a').each(function () {
        var href = String($(this).attr('href'));
        var numberSlip = href.split("=");
        var pageNumber = numberSlip[1];
        $(this).attr('href', '#');
        $(this).attr('rel', pageNumber);
        $(this).attr('class', 'btnPaging');
    });
}

$(document).on('change', '#ddlCategory_View', function () {
  //  alert('hii');
    GetFilteredIdeasView_ViewIdea(1);
});


function GetFilteredIdeasView_ViewIdea(page) {
    scrol = $(document).scrollTop();
    isNoScrol = true;
    //var keys = "";
    //$('#listIdeaKey option:selected').each(function () {
    //    keys += $(this).val() + ",";
    //});
    //var ideaStatusId = $('.ideaStatus').data('status');
    var ideaStatusId =  _ideaStatusId;
    var categoryId = $('#ddlCategory_View').val();
  
    currentPage = page;
    $.ajax({
        url: '/IdeaWatch/GetFilteredIdeaWatchViewList/',
        async: true,
        data: { page: currentPage, filterBy: _filterBy, keys: _ideaKeyId, ideaStatusId: ideaStatusId, categoryId: categoryId }
    }).done(function (data) {
        $('#ideaListView-container').html(data);
        RemoveHrefOfpaging();
        AddNicEditerOnDynamicTextArea();
        $(".mypopover").popover();

        if (parseInt(_filterBy) == 1) {
            $('#lblIdea-sortBy').html("Recently Added ");
        }
        if (parseInt(_filterBy) == 2) {
            $('#lblIdea-sortBy').html("Most Liked ");
        }
        if (parseInt(_filterBy) == 4) {
            $('#lblIdea-sortBy').html("Most commneted  ");
        }

        if (parseInt(ideaStatusId) == 2) {
            $('#lblIdea-sortBy').html("Pending Review");
        }

        $('#lblIdea').html(_ideaKeyName);


    }).fail(function () {
        callErrorMsg('There is some error please try again');
    });

    $(document).scroll(function () {
        if (isNoScrol == true) {
            $(document).scrollTop(scrol);
            isNoScrol = false;
        }
    });


    return false;
}

$(document).on('click', '#btnSearch', function () {
    var IdeaCode = $('#txtIdeaCode').val().trim();
    if (IdeaCode.length > 0) {
        var IdeaId = 0;
        window.location.href = "/IdeaWatch/IdeaWatchDeatil?ideaWatchId=" + IdeaId + " &ideaCode=" + IdeaCode;
    }
    else {
        callErrorMsg('IdeaWatch Code should not be blank !!');
    }
});