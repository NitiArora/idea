USE [FPIdeasWatch]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Role](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](300) NULL,
	[IsDeleted] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Replicated]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Replicated](
	[ReplicatedId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[ReplicatedId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RelatedTo]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RelatedTo](
	[RelatedToId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[RelatedToId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductType]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductType](
	[ProductTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[ProductTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PlatformType]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlatformType](
	[PlatformTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[PlatformTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PageName]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PageName](
	[PageNameId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[PageNameId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Location]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Location](
	[LocationId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[LocationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Category]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[CategoryId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[CategoryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BusinessObjective]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessObjective](
	[BusinessObjectiveId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[BusinessObjectiveId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BusinessImpact]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BusinessImpact](
	[BusinessImpactId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](300) NULL,
PRIMARY KEY CLUSTERED 
(
	[BusinessImpactId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdeaKey]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdeaKey](
	[keyId] [int] IDENTITY(1,1) NOT NULL,
	[keyName] [nvarchar](100) NULL,
 CONSTRAINT [PK_IdeaKey] PRIMARY KEY CLUSTERED 
(
	[keyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Department]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Department](
	[DepartmentId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[DepartmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IdeaStatus]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IdeaStatus](
	[IdeaStatusId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](300) NULL,
	[ParentId] [int] NULL,
	[StepId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdeaStatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[User]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](100) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ImageUrl] [nvarchar](500) NULL,
	[UserCode] [nvarchar](200) NULL,
	[Email] [nvarchar](200) NULL,
	[Alias] [nvarchar](200) NULL,
	[DepartmentId] [int] NULL,
	[SupervisorId] [int] NULL,
	[LocationId] [int] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRole]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[RoleId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[UserRoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdeasWatch]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[IdeasWatch](
	[IdeasWatchId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NULL,
	[Description] [nvarchar](max) NULL,
	[IsBug] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[ModifiedOn] [datetime] NULL,
	[IdeaStatusId] [int] NOT NULL,
	[StatusChangedBy] [int] NULL,
	[IdeaCode] [varchar](200) NULL,
	[IdeaCategoryId] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[RelatedToId] [int] NULL,
	[ReplicatedId] [int] NULL,
	[CurrentScenario] [varchar](500) NULL,
	[BenifitIdea] [varchar](500) NULL,
 CONSTRAINT [PK_IdeasWatch] PRIMARY KEY CLUSTERED 
(
	[IdeasWatchId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[IdeaWatch_ProductType]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdeaWatch_ProductType](
	[IdeaWatch_ProductTypeId] [int] IDENTITY(1,1) NOT NULL,
	[ProductTypeId] [int] NULL,
	[IdeasWatchId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdeaWatch_ProductTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdeaWatch_PlatformType]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdeaWatch_PlatformType](
	[IdeaWatch_PlatformTypeId] [int] IDENTITY(1,1) NOT NULL,
	[PlatformTypeId] [int] NULL,
	[IdeasWatchId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdeaWatch_PlatformTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdeaWatch_PageName]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdeaWatch_PageName](
	[IdeaWatch_PageNameId] [int] IDENTITY(1,1) NOT NULL,
	[PageNameId] [int] NULL,
	[IdeasWatchId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdeaWatch_PageNameId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdeaWatch_BusinessObjective]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdeaWatch_BusinessObjective](
	[IdeaWatch_BusinessObjectiveId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessObjectiveId] [int] NULL,
	[IdeasWatchId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdeaWatch_BusinessObjectiveId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdeaWatch_BusinessImpact]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdeaWatch_BusinessImpact](
	[IdeaWatch_BusinessImpactId] [int] IDENTITY(1,1) NOT NULL,
	[BusinessImpactId] [int] NULL,
	[IdeasWatchId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[IdeaWatch_BusinessImpactId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdeasWatch_Ideakey]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdeasWatch_Ideakey](
	[IdeasWatchkeyId] [int] IDENTITY(1,1) NOT NULL,
	[IdeasWatchId] [int] NOT NULL,
	[keyId] [int] NOT NULL,
 CONSTRAINT [PK_IdeasWatch_Ideakey] PRIMARY KEY CLUSTERED 
(
	[IdeasWatchkeyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdeaComment]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdeaComment](
	[CommentId] [int] IDENTITY(1,1) NOT NULL,
	[IdeasWatchId] [int] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[UserId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_IdeaComment] PRIMARY KEY CLUSTERED 
(
	[CommentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[FileAttachment]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileAttachment](
	[AttachmentId] [int] IDENTITY(1,1) NOT NULL,
	[IdeasWatchId] [int] NOT NULL,
	[Url] [nvarchar](500) NULL,
	[AddedOn] [datetime] NOT NULL,
	[FileName] [nvarchar](200) NULL,
	[FileContent] [image] NULL,
	[ContentType] [nvarchar](300) NULL,
	[FileType] [nvarchar](50) NULL,
 CONSTRAINT [PK_FileAttachment] PRIMARY KEY CLUSTERED 
(
	[AttachmentId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[IdeaLikeDislike]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[IdeaLikeDislike](
	[LikeDislikeId] [int] IDENTITY(1,1) NOT NULL,
	[IdeasWatchId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[IsLiked] [bit] NULL,
 CONSTRAINT [PK_IdeaLikeDislike] PRIMARY KEY CLUSTERED 
(
	[LikeDislikeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CommentReply]    Script Date: 03/02/2017 16:42:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CommentReply](
	[CommentReplyId] [int] IDENTITY(1,1) NOT NULL,
	[Detail] [nvarchar](max) NULL,
	[CommentId] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UserId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[CommentReplyId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Default [DF__IdeasWatc__IdeaS__5165187F]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeasWatch] ADD  DEFAULT ((1)) FOR [IdeaStatusId]
GO
/****** Object:  Default [DF__IdeasWatc__IsDel__5DCAEF64]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeasWatch] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
/****** Object:  ForeignKey [FK__CommentRe__Comme__25869641]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[CommentReply]  WITH CHECK ADD FOREIGN KEY([CommentId])
REFERENCES [dbo].[IdeaComment] ([CommentId])
GO
/****** Object:  ForeignKey [FK_CommentReply_User]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[CommentReply]  WITH CHECK ADD  CONSTRAINT [FK_CommentReply_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[CommentReply] CHECK CONSTRAINT [FK_CommentReply_User]
GO
/****** Object:  ForeignKey [FK_FileAttachment_IdeasWatch]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[FileAttachment]  WITH CHECK ADD  CONSTRAINT [FK_FileAttachment_IdeasWatch] FOREIGN KEY([IdeasWatchId])
REFERENCES [dbo].[IdeasWatch] ([IdeasWatchId])
GO
ALTER TABLE [dbo].[FileAttachment] CHECK CONSTRAINT [FK_FileAttachment_IdeasWatch]
GO
/****** Object:  ForeignKey [FK_IdeaComment_IdeasWatch]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaComment]  WITH CHECK ADD  CONSTRAINT [FK_IdeaComment_IdeasWatch] FOREIGN KEY([IdeasWatchId])
REFERENCES [dbo].[IdeasWatch] ([IdeasWatchId])
GO
ALTER TABLE [dbo].[IdeaComment] CHECK CONSTRAINT [FK_IdeaComment_IdeasWatch]
GO
/****** Object:  ForeignKey [FK_IdeaComment_User]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaComment]  WITH CHECK ADD  CONSTRAINT [FK_IdeaComment_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[IdeaComment] CHECK CONSTRAINT [FK_IdeaComment_User]
GO
/****** Object:  ForeignKey [FK_IdeaLikeDislike_IdeasWatch]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaLikeDislike]  WITH CHECK ADD  CONSTRAINT [FK_IdeaLikeDislike_IdeasWatch] FOREIGN KEY([IdeasWatchId])
REFERENCES [dbo].[IdeasWatch] ([IdeasWatchId])
GO
ALTER TABLE [dbo].[IdeaLikeDislike] CHECK CONSTRAINT [FK_IdeaLikeDislike_IdeasWatch]
GO
/****** Object:  ForeignKey [FK_IdeaLikeDislike_User]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaLikeDislike]  WITH CHECK ADD  CONSTRAINT [FK_IdeaLikeDislike_User] FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[IdeaLikeDislike] CHECK CONSTRAINT [FK_IdeaLikeDislike_User]
GO
/****** Object:  ForeignKey [fk_IdeasWatch_RelatedTo]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeasWatch]  WITH CHECK ADD  CONSTRAINT [fk_IdeasWatch_RelatedTo] FOREIGN KEY([RelatedToId])
REFERENCES [dbo].[RelatedTo] ([RelatedToId])
GO
ALTER TABLE [dbo].[IdeasWatch] CHECK CONSTRAINT [fk_IdeasWatch_RelatedTo]
GO
/****** Object:  ForeignKey [fk_IdeasWatch_Replicated]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeasWatch]  WITH CHECK ADD  CONSTRAINT [fk_IdeasWatch_Replicated] FOREIGN KEY([ReplicatedId])
REFERENCES [dbo].[Replicated] ([ReplicatedId])
GO
ALTER TABLE [dbo].[IdeasWatch] CHECK CONSTRAINT [fk_IdeasWatch_Replicated]
GO
/****** Object:  ForeignKey [FK_IdeasWatch_User]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeasWatch]  WITH CHECK ADD  CONSTRAINT [FK_IdeasWatch_User] FOREIGN KEY([CreatedBy])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[IdeasWatch] CHECK CONSTRAINT [FK_IdeasWatch_User]
GO
/****** Object:  ForeignKey [fk_IdeaWatch_StatusChangedBy]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeasWatch]  WITH CHECK ADD  CONSTRAINT [fk_IdeaWatch_StatusChangedBy] FOREIGN KEY([StatusChangedBy])
REFERENCES [dbo].[User] ([UserId])
GO
ALTER TABLE [dbo].[IdeasWatch] CHECK CONSTRAINT [fk_IdeaWatch_StatusChangedBy]
GO
/****** Object:  ForeignKey [fk_IdeaWatchCategory]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeasWatch]  WITH CHECK ADD  CONSTRAINT [fk_IdeaWatchCategory] FOREIGN KEY([IdeaCategoryId])
REFERENCES [dbo].[Category] ([CategoryId])
GO
ALTER TABLE [dbo].[IdeasWatch] CHECK CONSTRAINT [fk_IdeaWatchCategory]
GO
/****** Object:  ForeignKey [fk_IdeaWatchIdeaStatus]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeasWatch]  WITH CHECK ADD  CONSTRAINT [fk_IdeaWatchIdeaStatus] FOREIGN KEY([IdeaStatusId])
REFERENCES [dbo].[IdeaStatus] ([IdeaStatusId])
GO
ALTER TABLE [dbo].[IdeasWatch] CHECK CONSTRAINT [fk_IdeaWatchIdeaStatus]
GO
/****** Object:  ForeignKey [FK_IdeasWatch_Ideakey_IdeaKey]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeasWatch_Ideakey]  WITH CHECK ADD  CONSTRAINT [FK_IdeasWatch_Ideakey_IdeaKey] FOREIGN KEY([keyId])
REFERENCES [dbo].[IdeaKey] ([keyId])
GO
ALTER TABLE [dbo].[IdeasWatch_Ideakey] CHECK CONSTRAINT [FK_IdeasWatch_Ideakey_IdeaKey]
GO
/****** Object:  ForeignKey [FK_IdeasWatch_Ideakey_IdeasWatch]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeasWatch_Ideakey]  WITH CHECK ADD  CONSTRAINT [FK_IdeasWatch_Ideakey_IdeasWatch] FOREIGN KEY([IdeasWatchId])
REFERENCES [dbo].[IdeasWatch] ([IdeasWatchId])
GO
ALTER TABLE [dbo].[IdeasWatch_Ideakey] CHECK CONSTRAINT [FK_IdeasWatch_Ideakey_IdeasWatch]
GO
/****** Object:  ForeignKey [FK__IdeaWatch__Busin__412EB0B6]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaWatch_BusinessImpact]  WITH CHECK ADD FOREIGN KEY([BusinessImpactId])
REFERENCES [dbo].[BusinessImpact] ([BusinessImpactId])
GO
/****** Object:  ForeignKey [FK__IdeaWatch__Ideas__4222D4EF]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaWatch_BusinessImpact]  WITH CHECK ADD FOREIGN KEY([IdeasWatchId])
REFERENCES [dbo].[IdeasWatch] ([IdeasWatchId])
GO
/****** Object:  ForeignKey [FK__IdeaWatch__Busin__44FF419A]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaWatch_BusinessObjective]  WITH CHECK ADD FOREIGN KEY([BusinessObjectiveId])
REFERENCES [dbo].[BusinessObjective] ([BusinessObjectiveId])
GO
/****** Object:  ForeignKey [FK__IdeaWatch__Ideas__45F365D3]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaWatch_BusinessObjective]  WITH CHECK ADD FOREIGN KEY([IdeasWatchId])
REFERENCES [dbo].[IdeasWatch] ([IdeasWatchId])
GO
/****** Object:  ForeignKey [FK__IdeaWatch__Ideas__3E52440B]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaWatch_PageName]  WITH CHECK ADD FOREIGN KEY([IdeasWatchId])
REFERENCES [dbo].[IdeasWatch] ([IdeasWatchId])
GO
/****** Object:  ForeignKey [FK__IdeaWatch__PageN__3D5E1FD2]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaWatch_PageName]  WITH CHECK ADD FOREIGN KEY([PageNameId])
REFERENCES [dbo].[PageName] ([PageNameId])
GO
/****** Object:  ForeignKey [FK__IdeaWatch__Ideas__36B12243]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaWatch_PlatformType]  WITH CHECK ADD FOREIGN KEY([IdeasWatchId])
REFERENCES [dbo].[IdeasWatch] ([IdeasWatchId])
GO
/****** Object:  ForeignKey [FK__IdeaWatch__Platf__35BCFE0A]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaWatch_PlatformType]  WITH CHECK ADD FOREIGN KEY([PlatformTypeId])
REFERENCES [dbo].[PlatformType] ([PlatformTypeId])
GO
/****** Object:  ForeignKey [FK__IdeaWatch__Ideas__3A81B327]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaWatch_ProductType]  WITH CHECK ADD FOREIGN KEY([IdeasWatchId])
REFERENCES [dbo].[IdeasWatch] ([IdeasWatchId])
GO
/****** Object:  ForeignKey [FK__IdeaWatch__Produ__398D8EEE]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[IdeaWatch_ProductType]  WITH CHECK ADD FOREIGN KEY([ProductTypeId])
REFERENCES [dbo].[ProductType] ([ProductTypeId])
GO
/****** Object:  ForeignKey [fk_User_Department]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [fk_User_Department] FOREIGN KEY([DepartmentId])
REFERENCES [dbo].[Department] ([DepartmentId])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [fk_User_Department]
GO
/****** Object:  ForeignKey [fk_User_Location]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [fk_User_Location] FOREIGN KEY([LocationId])
REFERENCES [dbo].[Location] ([LocationId])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [fk_User_Location]
GO
/****** Object:  ForeignKey [FK__UserRole__RoleId__4CA06362]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleId])
GO
/****** Object:  ForeignKey [FK__UserRole__UserId__4BAC3F29]    Script Date: 03/02/2017 16:42:47 ******/
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD FOREIGN KEY([UserId])
REFERENCES [dbo].[User] ([UserId])
GO
