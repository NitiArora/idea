﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using FPIdeasWatch.Models;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using DataModel.Utility;

namespace FPIdeasWatch.Controllers
{

    public class AccountController : Controller
    {
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;

        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            return View();
        }
          [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(FPIdeasWatch.Models.LoginViewModel model, string returnUrl)
        {

            try
            {

                string userEmailId = AdAuth.Class1.SearchUser1(model.UserName, model.Password);
                if (Regex.IsMatch(userEmailId.ToLower(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z"))
                {
                    // AddUserAndMembership(userEmailId);
                    // return RedirectPageByRoleStatus();

                    DataModel.BO.UserDetail currentUser = new DataModel.BO.UserDetail();

                    // var accessToken = loginInfo.ExternalIdentity.Claims.Where(c => c.Type.Equals("urn:google:accesstoken")).Select(c => c.Value).FirstOrDefault();
                    //  Uri apiRequestUri = new Uri("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + accessToken);
                    //request profile image
                    //using (var webClient = new System.Net.WebClient())
                    //{
                    //    var json = webClient.DownloadString(apiRequestUri);
                    //    dynamic result1 = JsonConvert.DeserializeObject(json);
                    //    currentUser.ImageUrl = result1.picture;
                    //}



                    DataModel.BO.UserDetail user = BusinessLogicLayer.BLL.User.AddUser(userEmailId, "");

                    currentUser.UserId = user.UserId;
                    currentUser.UserName = userEmailId;
                    currentUser.LocationId = user.LocationId;
                    if (!user.IsSuperAdmin)
                    {
                        currentUser.IsSuperAdmin = DataModel.Utility.Base.SuperAdmin().Contains(currentUser.UserName);
                    }
                    currentUser.IsReviewPanelMember = user.IsReviewPanelMember;
                    DataModel.Utility.CurrentUser.CurrentUserDetail = currentUser;
                    return RedirectToAction("Index", "Dashboard");


                }
                else
                {
                    ModelState.AddModelError("", "The Domain Id or Password provided is incorrect or If you are unable to login, please contact to IT-Support");
                    Logger.WriteLogInfo("Response from AdAuth.dll for '" + model.UserName + "' : " + userEmailId);
                }
            }


            catch (Exception ex)
            {
                Logger.WriteLog("Error in AccountController > UserLogin(LoginModel model, string returnUrl)" + ex.ToString());
            }
            return View(model);
        }
        //[HttpPost]
        //[AllowAnonymous]
        //public ActionResult UserLogin(FPIdeasWatch.Models.LoginViewModel model)
        //{
        //    try
        //    {

        //        string userEmailId = AdAuth.Class1.SearchUser1(model.UserName, model.Password);
        //        if (Regex.IsMatch(userEmailId.ToLower(), @"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z"))
        //        {
        //           // AddUserAndMembership(userEmailId);
        //           // return RedirectPageByRoleStatus();

        //            DataModel.BO.UserDetail currentUser = new DataModel.BO.UserDetail();

        //           // var accessToken = loginInfo.ExternalIdentity.Claims.Where(c => c.Type.Equals("urn:google:accesstoken")).Select(c => c.Value).FirstOrDefault();
        //          //  Uri apiRequestUri = new Uri("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + accessToken);
        //            //request profile image
        //            //using (var webClient = new System.Net.WebClient())
        //            //{
        //            //    var json = webClient.DownloadString(apiRequestUri);
        //            //    dynamic result1 = JsonConvert.DeserializeObject(json);
        //            //    currentUser.ImageUrl = result1.picture;
        //            //}



        //            DataModel.BO.UserDetail user = BusinessLogicLayer.BLL.User.AddUser(userEmailId, "");

        //            currentUser.UserId = user.UserId;
        //            currentUser.UserName = userEmailId;
        //            currentUser.LocationId = user.LocationId;
        //            if (!user.IsSuperAdmin)
        //            {
        //                currentUser.IsSuperAdmin = DataModel.Utility.Base.SuperAdmin().Contains(currentUser.UserName);
        //            }
        //            currentUser.IsReviewPanelMember = user.IsReviewPanelMember;
        //            DataModel.Utility.CurrentUser.CurrentUserDetail = currentUser;
        //            return RedirectToAction("Index", "Dashboard");


        //        }
        //        else
        //        {
        //            ModelState.AddModelError("", "The Domain Id or Password provided is incorrect or If you are unable to login, please contact to IT-Support");
        //            Logger.WriteLogInfo("Response from AdAuth.dll for '" + model.UserName + "' : " + userEmailId);
        //        }
        //    }


        //    catch (Exception ex)
        //    {
        //        Logger.WriteLog("Error in AccountController > UserLogin(LoginModel model, string returnUrl)" + ex.ToString());
        //    }
        //    return View(model);
        //}

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            ControllerContext.HttpContext.Session.RemoveAll();
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }, null));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login", "Account");
            }

            var userEmail = loginInfo.Email.Split('@');
            if (userEmail[1] != "fareportal.com" && loginInfo.Email != "samtycoon1@gmail.com")
            {
                TempData["LoginError"] = "Failure, Kindly make sure you are using your fareportal gmail account.";
                return RedirectToAction("LoginFail", "Error");
            }
            else
            {
                DataModel.BO.UserDetail currentUser = new DataModel.BO.UserDetail();

                var accessToken = loginInfo.ExternalIdentity.Claims.Where(c => c.Type.Equals("urn:google:accesstoken")).Select(c => c.Value).FirstOrDefault();
                Uri apiRequestUri = new Uri("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + accessToken);
                //request profile image
                using (var webClient = new System.Net.WebClient())
                {
                    var json = webClient.DownloadString(apiRequestUri);
                    dynamic result1 = JsonConvert.DeserializeObject(json);
                    currentUser.ImageUrl = result1.picture;
                }



                DataModel.BO.UserDetail user = BusinessLogicLayer.BLL.User.AddUser(loginInfo.Email, currentUser.ImageUrl);

                currentUser.UserId = user.UserId;
                currentUser.UserName = loginInfo.Email;
                currentUser.LocationId = user.LocationId;
                if (!user.IsSuperAdmin)
                {
                    currentUser.IsSuperAdmin = DataModel.Utility.Base.SuperAdmin().Contains(currentUser.UserName);
                }
                currentUser.IsReviewPanelMember = user.IsReviewPanelMember;
                DataModel.Utility.CurrentUser.CurrentUserDetail = currentUser;
                return RedirectToAction("Index", "Dashboard");

            }


        }


        // POST: /Account/LogOff
        [AllowAnonymous]
        public ActionResult LogOff()
        {
            // AuthenticationManager.SignOut();
            Session.Abandon();
            return RedirectToAction("Login", "Account");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}