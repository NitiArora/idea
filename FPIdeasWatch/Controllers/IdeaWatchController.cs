﻿using FPIdeasWatch.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Web.Script.Serialization;
using DataModel.BO;
using BusinessLogicLayer.BLL;
using DataModel.Utility.Consts;
using System.Net;
using System.Threading;
using DataModel.Utility;


namespace FPIdeasWatch.Controllers
{
    [IsSessionExpired]
    public class IdeaWatchController : Controller
    {
        static Thread keepAliveThread = new Thread(KeepAlive);
        // GET: IdeaWatch      
        public ActionResult Index()
        {
            ViewBag.IsIdeaAdded = TempData["IsIdeaAdded"];
            ViewBag.IdeaAddedCode = TempData["IdeaAddedCode"];
            ViewBag.Active_AddIdea = "active";
            IdeasWatchList ideas = new IdeasWatchList();
            //if (DataModel.Utility.Consts.ThreadCountStatic.ThreadNumber == 1)
            //{
            //    keepAliveThread.Start();
            //    DataModel.Utility.Consts.ThreadCountStatic.ThreadNumber = 2;
            //}
            //IdeasWatchList ideas = IdeasWatch.GetFilteredIdeaViewModelList( FilteredBy.CreatedLast,IdeaStatus.Approved);
            ideas = IdeasWatch.GetFilteredIdeaBasedOnUserId();
            IPagedList<DataModel.BO.IdeasWatchDetail> listIdeas = ideas.IdeasWatchDetailList.ToPagedList(1, PagingSize.Page);

            DataModel.BO.IdeasWatchDetail ideasWatchDetail = new IdeasWatchDetail();

            ideasWatchDetail.BusinessImpactList = IdeasWatch.GetSelectListItemBusinessImpact();
            ideasWatchDetail.BusinessObjectiveList = IdeasWatch.GetSelectListItemBusinessObjective();
            ideasWatchDetail.PageNameList = IdeasWatch.GetSelectListItemPageName();
            ideasWatchDetail.PlatformTypeList = IdeasWatch.GetSelectListItemPlatformType();
            ideasWatchDetail.ProductTypeList = IdeasWatch.GetSelectListItemProductType();
            ideasWatchDetail.CategoryList = IdeasWatch.GetSelectListItemCategory();
            ideasWatchDetail.LocationList = IdeasWatch.GetSelectListItemLocation();
            ideasWatchDetail.ReplicatedList = IdeasWatch.GetSelectListItemReplicated();
            ideasWatchDetail.RelatedToList = IdeasWatch.GetSelectListItemRelatedTo();
            ViewBag.ListItemCategory = IdeasWatch.GetSelectListItemCategoryNotUsed();
            var tuple = Tuple.Create<DataModel.BO.IdeasWatchDetail, IPagedList<DataModel.BO.IdeasWatchDetail>>(ideasWatchDetail, listIdeas);
            return View(tuple);

        }

        public ActionResult ManageIdea()
        {

            IdeasWatchList ideas = IdeasWatch.GetFilteredIdeaViewModelList(FilteredBy.CreatedFirst, IdeaStatus.Approved);
            //if (ideas.IdeasWatchDetailList != null)
            //    ideas.IdeasWatchDetailList[0].DataForPageId = DataModel.Utility.Consts.IdeaPage.ManageIdea;
            return View(ideas.IdeasWatchDetailList.ToPagedList(1, PagingSize.Page));

        }


        [IsReviewer]
        public ActionResult PendingReview()
        {
            ViewBag.Active_PendingIdea = "active";

            ViewBag.IdeaStatusList = BusinessLogicLayer.BLL.CommonBLL.GetSelectListItemStatus();
            IdeasWatchList ideas = IdeasWatch.GetFilteredIdeaViewModelListForManageIdea(IdeaStatus.Pending);
            return View(ideas.IdeasWatchDetailList.ToPagedList(1, PagingSize.Page));

        }

        public ActionResult GetIdeaByStatusForManageIdea(int ideaStatusid)
        {
            ViewBag.Active_Dashboard = "active";
            ViewData["IdeaPage"] = DataModel.Utility.Consts.IdeaPage.ManageIdea;
            IdeasWatchList ideas = IdeasWatch.GetFilteredIdeaViewModelListForManageIdea(ideaStatusid);
            return PartialView("_Ideas", ideas.IdeasWatchDetailList.ToPagedList(1, PagingSize.Page));
        }

        [AllowAnonymous]
        static void KeepAlive()
        {
            var urlToActive = System.Configuration.ConfigurationManager.AppSettings["URLToActiveSite"];
            while (true)
            {
                WebRequest req = WebRequest.Create(urlToActive);

                try
                {
                    req.GetResponse();
                    Thread.Sleep(60000);
                }
                catch (ThreadAbortException ex)
                {

                }
            }
        }

        public ActionResult IdeaWatchDeatil(int ideaWatchId, string ideaCode = "0")
        {
            ViewBag.IsIdeaEdited = TempData["IsIdeaEdited"];
            ViewBag.EnableChangeStatus = true;
            IdeasWatchDetail ideaWatch = IdeasWatch.GetIdeasWatchDetail(ideaWatchId, ideaCode);
            if (ideaWatch != null)
                ideaWatch.DataForPageId = DataModel.Utility.Consts.IdeaPage.IdeaDetail;
            return View(ideaWatch);

        }
        //public JsonResult ApproveIdea(int ideaWatchId)
        //{
        //    var sr = IdeasWatch.ChangeIdeaStatus(ideaWatchId, DataModel.Utility.Consts.IdeaStatus.Approved);
        //    return Json(sr, JsonRequestBehavior.AllowGet);

        //}
        public JsonResult ChangeIdeaStatus(int ideaWatchId, int statusId)
        {
            var sr = ManageIdeaBLL.ChangeIdeaStatus(ideaWatchId, statusId);
            string sub =  " Idea "+sr.Result.IdeaCode+" status has been changed to " + sr.Result.Status ;
            List<string> emails = new List<string>();
            emails.Add(sr.Result.CreatedByEmail);
            FPIdeasWatch.Common.Mail.SendNotificationMail(sr.Result, sub, emails, CurrentUser.CurrentUserDetail.UserName);
            return Json(sr, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult RejectIdea(int ideaWatchId)
        //{
        //    var sr = IdeasWatch.ChangeIdeaStatus(ideaWatchId, DataModel.Utility.Consts.IdeaStatus.Rejected);
        //    return Json(sr,JsonRequestBehavior.AllowGet);

        //}

        public ActionResult ViewIdeasWatch(string keyId)
        {
            ViewBag.Active_ViewIdea = "active";
            IdeasWatchList ideas = IdeasWatch.GetFilteredIdeaViewModelList(FilteredBy.CreatedLast, IdeaStatus.Approved, 0, keyId);
            var categoryList = IdeasWatch.GetSelectListItemCategory();
            categoryList.RemoveAt(0);
            categoryList.Insert(0, new SelectListItem { Value = "0", Text = "-- All --" });
            ViewBag.CategoryList = categoryList;
            return View(ideas.IdeasWatchDetailList.ToPagedList(1, PagingSize.Page));
        }

        [HttpPost]
        [ValidateInput(false)]

        public ActionResult AddIdea(DataModel.BO.IdeasWatchDetail model, List<HttpPostedFileBase> fileUpload)
        {
            if (model.IdeasWatchId > 0)
            {
                var srUpdate = IdeasWatch.UpdateIdeaWatch(model);
                if (srUpdate.IsSuccessful)
                {
                    TempData["IsIdeaEdited"] = 1;
                    
                    
                    return RedirectToAction("IdeaWatchDeatil", "IdeaWatch", new { ideaWatchId = model.IdeasWatchId });
                }
                else
                {
                    TempData["fail"] = "There are some error found !";
                    return RedirectToAction("ErrorDetails", "Error");
                }
            }
            var sr = IdeasWatch.AddIdeaWatch(model);
            if (sr.IsSuccessful)
            {
                if (fileUpload[0] != null)
                {

                    //List<string> EmailIds = new List<string>();
                    //EmailIds.Add("rana.singh@fareportal.com");
                    //string mailBody = CommonBLL.PopulateHtmlFormattedBody(" New Idea has been Added", "This is auto generated email.<br>Please check");
                    //Helper.SendMailToList(EmailIds, mailBody);
                    List<Dictionary<string, string>> expando = new List<Dictionary<string, string>>();

                    //var mapPath = Common.Common.SeverPathOfUploadedFile();
                    expando = Common.Common.CreateMediaPath(fileUpload);
                    try
                    {
                    if (expando.Count > 0)
                    {
                        // foreach (HttpPostedFileBase item in fileUpload)
                        //for (int i = 0; i < fileUpload.Count - 1; i++)
                        //{

                            foreach (Dictionary<string, string> item in expando)
                            {
                                string fullPath = string.Empty;
                                string fileName = string.Empty;
                                //var item = fileUpload[i];
                                if (item != null)
                                {
                                    //if (Array.Exists(model.FilesToBeUploaded.Split(','), s => s.Equals(item.FileName)))
                                    //{
                                    if (item.FirstOrDefault(x => x.Key == "FileName").Value != null)
                                    {
                                        DataModel.Model.FileAttachment obj = new DataModel.Model.FileAttachment();
                                        obj.IdeasWatchId = sr.Result.IdeasWatchId;
                                        obj.FileName = item["FileName"]; 
                                        obj.AddedOn = DataModel.Utility.Base.GetCurrentIndianDateTIme();
                                        obj.ContentType = item["ContentType"];
                                        obj.FileType = item["FileType"]; 
                                        //using (var reader = new System.IO.BinaryReader(item.InputStream))
                                        //{
                                        //    obj.FileContent = reader.ReadBytes(item.ContentLength);
                                        //}
                                        obj.FileContent = null;
                                        IdeasWatch.AddIdeaAttachement(obj);
                                        //}
                                    }
                                }
                            }
                        //}
                        }
                        string sub = "New Idea #" + sr.Result.IdeaCode + " created";
                        List<string> emails = BusinessLogicLayer.BLL.User.GetEmailListOfReviewPanel();
                        FPIdeasWatch.Common.Mail.SendNotificationMail(sr.Result, sub, emails, "None");
                    }
                    catch (Exception ex)
                    {
                        TempData["fail"] = ex.Message + " -- innser exception -- " + ex.InnerException;
                        return RedirectToAction("ErrorDetails", "Error");

                    }
                }
                TempData["IdeaAddedCode"] = sr.Result.IdeaCode.ToString();
            }
            else
            {
                TempData["fail"] = sr.Message;
                return RedirectToAction("ErrorDetails", "Error");
            }
            TempData["IsIdeaAdded"] = 1;
            return RedirectToAction("Index", "IdeaWatch");
        }
       
        public ActionResult AddIdeaLikeDislike(int IdeaWatchId, bool isLiked)
        {
            DataModel.Model.IdeaLikeDislike obj = new DataModel.Model.IdeaLikeDislike();
            obj.IdeasWatchId = IdeaWatchId;
            obj.IsLiked = isLiked;
            obj.UserId = DataModel.Utility.CurrentUser.CurrentUserDetail.UserId;
            IdeasWatch.AddIdeaLikeDislike(obj);
            IdeasWatchDetail ideas = IdeasWatch.GetIdeasWatchDetail(IdeaWatchId);
            return PartialView("_IdeaLikeUnlike", ideas);

        }
        public ActionResult GetIdeaWatchDetails(int page)
        {
            IdeasWatchList ideas = IdeasWatch.GetFilteredIdeaViewModelList(FilteredBy.CreatedLast, IdeaStatus.Approved);
            return PartialView("_Ideas", ideas.IdeasWatchDetailList.ToPagedList(page, 10));
        }
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddComent(string txtComments, int ideaWatchId)
        {
            DataModel.Model.IdeaComment comment = new DataModel.Model.IdeaComment();
            comment.CreatedOn = DataModel.Utility.Base.GetCurrentIndianDateTIme();
            comment.Comment = txtComments;
            comment.UserId = DataModel.Utility.CurrentUser.CurrentUserDetail.UserId;
            comment.IdeasWatchId = ideaWatchId;
            IdeasWatch.AddIdeaComments(comment);
            List<DataModel.BO.CommentDetail> comments = IdeasWatch.GetCommentsByIdeaWatchId(ideaWatchId);
            return PartialView("_Comments", comments);
        }
        public ActionResult GetFilteredIdeaWatchViewList(int page, int filterBy, string keys, int ideaStatusId, int categoryId = 0)
        {
            ideaStatusId = DataModel.Utility.Consts.IdeaStatus.Approved;
            //if(filterBy==DataModel.Utility.Consts.FilteredBy.CreatedLast)
            //{
            //    ViewBag.RecentlyAddedActive = "active";
            //}
            //else if (filterBy == DataModel.Utility.Consts.FilteredBy.MostLiked)
            //{
            //    ViewBag.MostLikedActive = "active";
            //}

            //else if (filterBy == DataModel.Utility.Consts.FilteredBy.MostComments)
            //{
            //    ViewBag.MostCommentsActive = "active";
            //}


            IdeasWatchList ideas = IdeasWatch.GetFilteredIdeaViewModelList(filterBy, ideaStatusId, categoryId, keys);
            PagedList<DataModel.BO.IdeasWatchDetail> records = new PagedList<DataModel.BO.IdeasWatchDetail>(ideas.IdeasWatchDetailList, page, PagingSize.Page);// ideas.IdeasWatchDetailList.ToPagedList(page, 10);
            return PartialView("_Ideas", records);
        }
        public ActionResult GetIdeaWatchDetailByIdeaCode(string ideaCode)
        {
            var idea = BusinessLogicLayer.BLL.IdeasWatch.GetIdeasWatchDetailByIdeaCode(ideaCode);
            return RedirectToAction("IdeaWatchDeatil", idea);
        }
        public ActionResult GetIdeaListForManageIdeaOnPaging(int page, int ideaStatusId)
        {
            ViewData["IdeaPage"] = DataModel.Utility.Consts.IdeaPage.ManageIdea;
            IdeasWatchList ideas = ManageIdeaBLL.GetIdeasListForManageIdea(ideaStatusId);
            PagedList<DataModel.BO.IdeasWatchDetail> records = new PagedList<DataModel.BO.IdeasWatchDetail>(ideas.IdeasWatchDetailList, page, PagingSize.Page);// ideas.IdeasWatchDetailList.ToPagedList(page, 10);
            return PartialView("_Ideas", records);
        }
        public ActionResult PreviewFile(int attachmentId)
        {
            //var fileToRetrieve = db.Files.Find(id);
            DataModel.Model.FileAttachment fileToRetrieve = IdeasWatch.GetFileAttachment(attachmentId);
            fileToRetrieve.Url = Base.BaseImageUrl + fileToRetrieve.FileName;
            return File(fileToRetrieve.Url, fileToRetrieve.ContentType);
            //  return new FileStreamResult(new System.IO.MemoryStream(fileToRetrieve.FileContent), fileToRetrieve.ContentType);
            // fileToRetrieve.FileContent


        }
        public string DownloadFile(int attachmentId)
        {
            DataModel.Model.FileAttachment fileToRetrieve = IdeasWatch.GetFileAttachment(attachmentId);
            byte[] photo = fileToRetrieve.FileContent;
            string imageSrc = null;
            if (photo != null)
            {
                MemoryStream ms = new MemoryStream();
                ms.Write(photo, 78, photo.Length - 78);
                string imageBase64 = Convert.ToBase64String(ms.ToArray());
                imageSrc = string.Format("data:image/jpeg;base64,{0}", imageBase64);
            }

            // i tried his way of selecting the right record and preforming the toArray method in the return statment 
            // but it kept giving me an error about converting linq.binary to byte[] tried a cast that didnt work so i came up with this

            // byte[] imageData = fileToRetrieve.FileContent.ToArray();
            return imageSrc;
        }
        public ActionResult DeleteIdea(int ideaWatchId, int page)
        {
            var result = IdeasWatch.DeleteIdea(ideaWatchId);
            ViewBag.IsViewOnly = false;
            return GetFilteredIdeaWatchViewList(page, FilteredBy.CreatedLast, "", DataModel.Utility.Consts.IdeaStatus.Approved, 0);
        }

        public ActionResult EditIdea(int ideaWatchId)
        {
            IdeasWatchDetail ideasWatchDetail = IdeasWatch.GetIdeasWatchDetail(ideaWatchId);
            ideasWatchDetail.BusinessImpactList = IdeasWatch.GetSelectListItemBusinessImpact();
            ideasWatchDetail.BusinessObjectiveList = IdeasWatch.GetSelectListItemBusinessObjective();
            ideasWatchDetail.PageNameList = IdeasWatch.GetSelectListItemPageName();
            ideasWatchDetail.PlatformTypeList = IdeasWatch.GetSelectListItemPlatformType();
            ideasWatchDetail.ProductTypeList = IdeasWatch.GetSelectListItemProductType();
            ideasWatchDetail.CategoryList = IdeasWatch.GetSelectListItemCategory();
            ideasWatchDetail.LocationList = IdeasWatch.EditSelectListItemLocation();
            ideasWatchDetail.ReplicatedList = IdeasWatch.GetSelectListItemReplicated();
            ideasWatchDetail.RelatedToList = IdeasWatch.GetSelectListItemRelatedTo();
            return View(ideasWatchDetail);
        }
        public ActionResult AddCommentReply(string txtReply, int commentId)
        {
            DataModel.Model.CommentReply commentReply = new DataModel.Model.CommentReply();
            commentReply.CreatedOn = DataModel.Utility.Base.GetCurrentIndianDateTIme();
            commentReply.Detail = txtReply;
            commentReply.UserId = DataModel.Utility.CurrentUser.CurrentUserDetail.UserId;
            commentReply.CommentId = commentId;
            IdeasWatch.AddCommentReply(commentReply);
            List<DataModel.BO.CommentReplyDetail> commentReplies = IdeasWatch.GetCommentReplyByCommentId(commentId);
            return PartialView("_CommnetReply", commentReplies);
        }
        [HttpPost]
        public JsonResult DeleteComment(int commentId)
        {

            var result = BusinessLogicLayer.BLL.IdeasWatch.DeleteComment(commentId);
            return Json(result);
        }
        [HttpPost]
        public JsonResult DeleteCommentReply(int replyCommentId)
        {
            var result = BusinessLogicLayer.BLL.IdeasWatch.DeleteCommentReply(replyCommentId);
            return Json(result);
        }
        public ActionResult GetKeyCloudList()
        {
            List<DataModel.BO.KeyCloud> keyCloud = IdeasWatch.GetKeyCloudList();
            var json = new JavaScriptSerializer().Serialize(keyCloud);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AddNewCategory(string categoryName)
        {
            DataModel.Model.Category objCategory = new DataModel.Model.Category();
            objCategory.Name = categoryName;
            var result = IdeasWatch.AddNewCategory(objCategory);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveCategory(int categoryId)
        {
            var result = BusinessLogicLayer.BLL.IdeasWatch.DeleteCategory(categoryId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetIdeaListForAddIdeaOnPaging(int page)
        {
            ViewData["IdeaPage"] = DataModel.Utility.Consts.IdeaPage.AddIdea;
            IdeasWatchList ideas = IdeasWatch.GetFilteredIdeaBasedOnUserId();
            PagedList<DataModel.BO.IdeasWatchDetail> records = new PagedList<DataModel.BO.IdeasWatchDetail>(ideas.IdeasWatchDetailList, page, PagingSize.Page);// ideas.IdeasWatchDetailList.ToPagedList(page, 10);
            return PartialView("_Ideas", records);
        }
    }
}