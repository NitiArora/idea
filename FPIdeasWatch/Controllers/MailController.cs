﻿using ActionMailer.Net.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPIdeasWatch.Controllers
{
    public static class ExtensionMethods
    {
        public static string ToPublicUrl(this UrlHelper urlHelper, Uri relativeUri)
        {
            var httpContext = urlHelper.RequestContext.HttpContext;

            var uriBuilder = new UriBuilder
            {
                Host = httpContext.Request.Url.Host,
                Path = "/",
                Port = 80,
                Scheme = "http",
            };

            if (httpContext.Request.IsLocal)
            {
                uriBuilder.Port = httpContext.Request.Url.Port;
            }

            return new Uri(uriBuilder.Uri, relativeUri).AbsoluteUri;
        }
    }

    public class MailController : MailerBase
    {
        public EmailResult NewComment(string release, string comment)
        {
            //if (release.ReleaseLikes == null || release.ReleaseLikes.Count == 0)
            //    return null;
            //  To.Add("releasemanager@fareportal.com");
            To.Add("ritesh.kumar@fareportal.com");
            To.Add("narora@fareportal.com ");
            //foreach (var item in release.ReleaseLikes)
            //{
            //    BCC.Add(item.UserName);
            //}
            Subject = "New comment: release";

            var urlhelper = new UrlHelper(base.HttpContextBase.Request.RequestContext);
            ViewBag.ReleasePageUrl = urlhelper.Action("Release", "Home", new { id =1});
            ViewBag.ReleasePageUrl = urlhelper.ToPublicUrl(new Uri(ViewBag.ReleasePageUrl, UriKind.Relative));

            //return null;

            return Email("NewComment", comment);
        }
    }
}