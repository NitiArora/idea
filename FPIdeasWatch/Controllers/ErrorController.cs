﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FPIdeasWatch.Controllers
{
    public class ErrorController : Controller
    {
        // GET: Error
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SessionExpired()
        {
            return View();
        }
        public ActionResult LoginFail()
        {
            return View();
        }
        public ActionResult ErrorDetails()
        {
            ViewBag.Fail = TempData["fail"];
            return View();
        }
        public ViewResult UnauthorizedAccess()
        {
            Response.StatusCode = 403;
            return View("UnauthorizedAccess");
        }
    }
}