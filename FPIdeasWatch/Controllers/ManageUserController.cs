﻿using FPIdeasWatch.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessLogicLayer.BLL;
using DataModel.Utility;

namespace FPIdeasWatch.Controllers
{
    [IsSessionExpired]
    [IsSuperAdmin]
    public class ManageUserController : Controller
    {
        // GET: ManageUser
        public ActionResult Index()
        {
            ViewBag.Active_ManagerUser = "active";
            var userOfReviewpanel = BusinessLogicLayer.BLL.User.GetUserOfReviewPanel();
            var userWithoutReviewpanel = BusinessLogicLayer.BLL.User.GetUserWithoutReviewPanel();
            var superAdminList = DataModel.Utility.Base.SuperAdmin().OrderBy(x=>x).ToList();
            var tuple = Tuple.Create<List<DataModel.BO.UserDetail>, List<DataModel.BO.UserDetail>, List<string>>(userOfReviewpanel, userWithoutReviewpanel, superAdminList);
            return View(tuple);
        }

        public JsonResult AddingMembersInReviewPanel(int[] userId)
        {
            var result = BusinessLogicLayer.BLL.User.AssignRole(userId,DataModel.Utility.Consts.Role.ReviewPanelId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveMemberFromReviewPanel(int userId)
        {
            var result = BusinessLogicLayer.BLL.User.RemoveRole(userId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}