﻿using BusinessLogicLayer.BLL;
using DataModel.BO;
using DataModel.Utility.Consts;
using FPIdeasWatch.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using PagedList;

namespace FPIdeasWatch.Controllers
{
    [IsSessionExpired]
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            ViewBag.Active_Dashboard = "active";
            IdeasWatchList ideas = Dashboard.GetIdeasListForDashboard(IdeaStatus.Approved);
            ideas.IdeasWatchDetailList.ToPagedList(1,PagingSize.Page);
            return View(ideas);
        }
        public JsonResult GetIdeaStatusDataForChartData()
        {
            List<DataModel.BO.ChartView> result = BusinessLogicLayer.BLL.Dashboard.GetIdeaStatusForChartData();           
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetIdeaWithStatusForDashboard(int ideaStatusid)
        {
            ViewBag.Active_Dashboard = "active";
            IdeasWatchList ideas = Dashboard.GetIdeasListForDashboard(ideaStatusid);
            return PartialView("_IdeaListDashboard", ideas);
        }
        public ActionResult IdeaList(string ideaStatusId="1")
        {
            ViewBag.Active_Dashboard = "active";
            ViewBag.IdeaList = "IdeaList";
            IdeasWatchList ideas = Dashboard.GetIdeasForIdeaList(Convert.ToInt32(ideaStatusId));
            return View(ideas);
        }

        public ActionResult GetIdeaWithStatusForIdeaList(string ideaStatusId = "1")
        {
            IdeasWatchList ideas = Dashboard.GetIdeasForIdeaList(Convert.ToInt32(ideaStatusId));
            return PartialView("_IdeaListDashboard", ideas);
        }
    }
}