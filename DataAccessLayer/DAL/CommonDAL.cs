﻿using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DAL
{
    public class CommonDAL
    {
        public static List<DataModel.Model.IdeaStatu> StatusList() 
        {
            List<DataModel.Model.IdeaStatu> status = new List<DataModel.Model.IdeaStatu>();
            using (var context = new FPIdeasWatchEntities())
            {
                status = (from a in context.IdeaStatus select a).ToList();
            }
            return status;
        }
    }
}
