﻿using DataModel.Model;
using DataModel.Utility;
using DataModel.Utility.Consts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DAL
{
    public class IdeasWatch
    {
     
        public static bool AddIdeasWatchIdeakey(string keys, int ideaWatchId)
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {

                    string[] keyArray = keys.Split(',');
                    foreach (string key in keyArray)
                    {
                        if (!string.IsNullOrEmpty(key))
                        {
                            int keyId = 0;
                            string trimKey = key.Trim();
                            var existKey = (from a in context.IdeaKeys where a.keyName == trimKey select a).FirstOrDefault();
                            if (existKey == null)
                            {
                                DataModel.Model.IdeaKey IdeaKey = new IdeaKey();
                                IdeaKey.keyName = trimKey;
                                context.IdeaKeys.Add(IdeaKey);
                                context.SaveChanges();
                                keyId = IdeaKey.keyId;
                            }
                            else
                            {
                                keyId = existKey.keyId;
                            }
                            var existRecords = (from a in context.IdeasWatch_Ideakey where a.keyId == keyId && a.IdeasWatchId == ideaWatchId select a).FirstOrDefault();
                            if (existRecords == null)
                            {
                                DataModel.Model.IdeasWatch_Ideakey obj = new IdeasWatch_Ideakey();
                                obj.keyId = keyId;
                                obj.IdeasWatchId = ideaWatchId;
                                context.IdeasWatch_Ideakey.Add(obj);
                                context.SaveChanges();
                            }
                        }

                    }
                   DataModel.Utility.Base.ResetCache();
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }
        public static DataModel.BO.ServiceReturnType.ServiceResult<DataModel.BO.IdeasWatchDetail> AddIdeaWatch(DataModel.BO.IdeasWatchDetail ideaWatchDeatil)
        {
            DataModel.BO.IdeasWatchDetail obj = new DataModel.BO.IdeasWatchDetail();
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {
                    DataModel.Model.IdeasWatch ideasWatch = new DataModel.Model.IdeasWatch();
                    ideasWatch.Title = ideaWatchDeatil.Title;
                    ideasWatch.Description = ideaWatchDeatil.Description;
                    ideasWatch.IsBug = ideaWatchDeatil.IsBug;
                    ideasWatch.CreatedBy = DataModel.Utility.CurrentUser.CurrentUserDetail.UserId;
                    ideasWatch.CreatedOn = DataModel.Utility.Base.GetCurrentIndianDateTIme();// DateTime.Now;
                    ideasWatch.IdeaStatusId = DataModel.Utility.Consts.IdeaStatus.Pending;
                    ideasWatch.IdeaCategoryId = ideaWatchDeatil.IdeaCategoryId;
                    ideasWatch.ReplicatedId = ideaWatchDeatil.ReplicatedId;
                    ideasWatch.RelatedToId = ideaWatchDeatil.RelatedToId;
                    ideasWatch.CurrentScenario = ideaWatchDeatil.CurrentScenario;
                    ideasWatch.BenifitIdea = ideaWatchDeatil.BenifitIdea;
                    ideasWatch.IsDeleted = false;

                    context.IdeasWatches.Add(ideasWatch);
                    context.SaveChanges();
                 
                    int ideaWatchId = ideasWatch.IdeasWatchId;
                    ideasWatch.IdeaCode =  DataModel.Utility.Consts.IdeaCode.Code+ ideaWatchId ;
                    context.SaveChanges();
                    obj.IdeaCode= ideasWatch.IdeaCode;
                    obj.Status = context.IdeaStatus.Where(a => a.IdeaStatusId == DataModel.Utility.Consts.IdeaStatus.Pending).FirstOrDefault().Name;
                    obj.Title = ideasWatch.Title;
                    obj.CreatedByEmail = CurrentUser.CurrentUserDetail.UserName;
                    obj.IdeasWatchId = ideasWatch.IdeasWatchId;

                    if (ideaWatchDeatil.BusinessImpactId > 0)
                    {
                        DataModel.Model.IdeaWatch_BusinessImpact ideaWatch_BusinessImpact = new IdeaWatch_BusinessImpact();
                        ideaWatch_BusinessImpact.IdeasWatchId = ideaWatchId;
                        ideaWatch_BusinessImpact.BusinessImpactId = ideaWatchDeatil.BusinessImpactId;
                        context.IdeaWatch_BusinessImpact.Add(ideaWatch_BusinessImpact);
                    }

                    if (ideaWatchDeatil.PlatformTypeId > 0)
                    {
                        DataModel.Model.IdeaWatch_PlatformType ideaWatch_PlatformType = new IdeaWatch_PlatformType();
                        ideaWatch_PlatformType.IdeasWatchId = ideaWatchId;
                        ideaWatch_PlatformType.PlatformTypeId = ideaWatchDeatil.PlatformTypeId;
                        context.IdeaWatch_PlatformType.Add(ideaWatch_PlatformType);
                    }
                    if (ideaWatchDeatil.ProductTypeId > 0)
                    {
                        DataModel.Model.IdeaWatch_ProductType ideaWatch_ProductType = new IdeaWatch_ProductType();
                        ideaWatch_ProductType.IdeasWatchId = ideaWatchId;
                        ideaWatch_ProductType.ProductTypeId = ideaWatchDeatil.ProductTypeId;
                        context.IdeaWatch_ProductType.Add(ideaWatch_ProductType);
                    }
                    if (ideaWatchDeatil.PageNameId > 0)
                    {
                        DataModel.Model.IdeaWatch_PageName ideaWatch_PageName = new IdeaWatch_PageName();
                        ideaWatch_PageName.IdeasWatchId = ideaWatchId;
                        ideaWatch_PageName.PageNameId = ideaWatchDeatil.PageNameId;
                        context.IdeaWatch_PageName.Add(ideaWatch_PageName);
                    }
                    if (ideaWatchDeatil.BusinessObjectiveId > 0)
                    {
                        DataModel.Model.IdeaWatch_BusinessObjective ideaWatch_BusinessObjective = new IdeaWatch_BusinessObjective();
                        ideaWatch_BusinessObjective.IdeasWatchId = ideaWatchId;
                        ideaWatch_BusinessObjective.BusinessObjectiveId = ideaWatchDeatil.BusinessObjectiveId;
                        context.IdeaWatch_BusinessObjective.Add(ideaWatch_BusinessObjective);
                    }
                    if (DataModel.Utility.CurrentUser.CurrentUserDetail.LocationId == null)
                    {
                        var username = (from a in context.Users where a.UserId == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId select a).FirstOrDefault();
                        if (username.UserId != null)
                        {
                            username.LocationId = ideaWatchDeatil.LocationId;
                            DataModel.Utility.CurrentUser.CurrentUserDetail.LocationId = ideaWatchDeatil.LocationId;
                        }
                    }
                    context.SaveChanges();


                    return DataModel.BO.ServiceReturnType.SuccessWithDataResult<DataModel.BO.IdeasWatchDetail>(obj);
                }
            }
            catch (Exception ex)
            {
                var result = DataModel.BO.ServiceReturnType.FailureWithDataResult<DataModel.BO.IdeasWatchDetail>(obj);
                result.Message ="Error !" +ex.Message +" Inner Exception !"+ex.InnerException;
                return result;
            }
        }
        public static int AddIdeaLikeDislike(DataModel.Model.IdeaLikeDislike obj)
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    DataModel.Utility.Base.ResetCache();
                    var existRecord = (from a in context.IdeaLikeDislikes where a.IdeasWatchId == obj.IdeasWatchId && a.UserId == obj.UserId select a).FirstOrDefault();
                    if (existRecord == null)
                    {
                        context.IdeaLikeDislikes.Add(obj);
                        context.SaveChanges();
                        return obj.IdeasWatchId;
                    }
                    else
                    {
                        existRecord.IsLiked = obj.IsLiked;
                        context.SaveChanges();
                        return existRecord.IdeasWatchId;
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static int AddIdeaWatchAssIdeaKey(DataModel.Model.IdeasWatch_Ideakey obj)
        {
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {
                    context.IdeasWatch_Ideakey.Add(obj);
                    context.SaveChanges();
                    return obj.IdeasWatchId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static int AddIdeaComments(DataModel.Model.IdeaComment obj)
        {
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {
                    context.IdeaComments.Add(obj);
                    context.SaveChanges();
                    return obj.CommentId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static int AddIdeaAttachement(DataModel.Model.FileAttachment obj)
        {
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {
                    context.FileAttachments.Add(obj);
                    context.SaveChanges();
                    return obj.IdeasWatchId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static int AddCommentReply(DataModel.Model.CommentReply obj)
        {
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {
                    context.CommentReplies.Add(obj);
                    context.SaveChanges();
                    return obj.CommentReplyId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static List<DataModel.BO.IdeasWatchDetail> GetIdeasWatchDetailList()
        {
            try
            {
                List<DataModel.BO.IdeasWatchDetail> ideas = new List<DataModel.BO.IdeasWatchDetail>();
                List<DataModel.BO.IdeasWatchDetail> airlineAircraftsListCache = (List<DataModel.BO.IdeasWatchDetail>)DataModel.Utility.Base.GetMyCachedItem(DataModel.Utility.Consts.CacheDetail.Cache_IdeaWatchList);
                if (airlineAircraftsListCache != null)
                {
                    ideas = airlineAircraftsListCache;

                }
                else
                {
                    using (var context = new FPIdeasWatchEntities())
                    {
                        var fileAttachment = new List<DataModel.Model.FileAttachment>();
                        ideas = (from a in context.IdeasWatches
                                 where !a.IsDeleted
                                 select new DataModel.BO.IdeasWatchDetail
                                 {
                                     IdeaCode = a.IdeaCode,
                                     IdeasWatchId = a.IdeasWatchId,
                                     CurrentStatusId = a.IdeaStatusId,
                                     CurrentStatusName = a.IdeaStatu.Name,
                                     CurrentStepId = a.IdeaStatu.StepId ?? 0,
                                     Title = a.Title,
                                     Description = a.Description,
                                     UserName = a.User.UserName,
                                     CreatedOn = a.CreatedOn,
                                     IsBug = a.IsBug,
                                     IdeaStatusId = a.IdeaStatusId,
                                     CommentDetailList = (from b in context.IdeaComments where b.IdeasWatchId == a.IdeasWatchId select new DataModel.BO.CommentDetail() { IdeasWatchId = b.IdeasWatchId, Comment = b.Comment, UserName = b.User.UserName, CreatedOn = b.CreatedOn, ImageUrl = b.User.ImageUrl }).ToList(),
                                     AttachmentList = fileAttachment,// a.FileAttachments.ToList(), // context.FileAttachments.Where(b => b.IdeasWatchId == a.IdeasWatchId).Select(c => c).ToList(), //        
                                     AttachmentCount = a.FileAttachments.Where(b => b.FileContent == null).ToList().Count,
                                     Status = a.IdeaStatu.Name,
                                     IdeaCategoryId = a.IdeaCategoryId,
                                     KeyList = a.IdeasWatch_Ideakey.Where(b => b.IdeasWatchId == a.IdeasWatchId).Select(c => c.IdeaKey).ToList(),
                                     KeyIdList = a.IdeasWatch_Ideakey.Where(b => b.IdeasWatchId == a.IdeasWatchId).Select(c => c.keyId).ToList(),
                                     LikedUserList = a.IdeaLikeDislikes.Where(b => b.IdeasWatchId == a.IdeasWatchId && b.IsLiked == true).Select(c => c.User.UserName).ToList(),
                                     // UnlikedUserList = _allIdeaLikeDislike.Where(b => b.IdeasWatchId == a.IdeasWatchId && b.IsLiked == false).Select(c => c.User.UserName).ToList(),
                                     //  IsLikedByCuurentUser = a.IdeaLikeDislikes.Where(b => b.IdeasWatchId == a.IdeasWatchId && b.IsLiked == true && b.UserId == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId).ToList().Count > 0,
                                     //  IsDislikedByCuurentUser = _allIdeaLikeDislike.Where(b => b.IdeasWatchId == a.IdeasWatchId && b.IsLiked == false && b.UserId == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId).ToList().Count > 0,
                                     //  IsEditPermission = a.CreatedBy == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId,
                                     IdeaLikeDislikeList = a.IdeaLikeDislikes.Where(b => b.IsLiked == true).ToList(),
                                     CreatedBy = a.CreatedBy,
                                     StatusChangedBy = a.User1.UserId,
                                     StatusChangedByName = a.User1.UserName,
                                     PageNameModel = a.IdeaWatch_PageName.Select(b => b.PageName).FirstOrDefault(),
                                     ProductTypeModel = a.IdeaWatch_ProductType.Select(b => b.ProductType).FirstOrDefault(),
                                     PlatformTypeModel = a.IdeaWatch_PlatformType.Select(b => b.PlatformType).FirstOrDefault(),
                                     BusinessImpactModel = a.IdeaWatch_BusinessImpact.Select(b => b.BusinessImpact).FirstOrDefault(),
                                     BusinessObjectiveModel = a.IdeaWatch_BusinessObjective.Select(b => b.BusinessObjective).FirstOrDefault(),
                                     NextStatusList =  (from b in context.IdeaStatus where b.StepId == (a.IdeaStatu.StepId + 1) && a.IdeaStatusId != DataModel.Utility.Consts.IdeaStatus.Rejected select new DataModel.BO.NextStatus() { StatusId = b.IdeaStatusId, StatusName = b.Name }).ToList(),
                                     ParentId=a.IdeaStatu.ParentId??0,
                                 }).ToList();

                        

                        DataModel.Utility.Base.AddCache(DataModel.Utility.Consts.CacheDetail.Cache_IdeaWatchList, ideas);
                     



                    }
                }
                foreach(var item in ideas)
                {
                    item.IsLikedByCuurentUser = item.IdeaLikeDislikeList.Where(b => b.UserId == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId).FirstOrDefault()==null?false:true;
                    item.IsEditPermission = item.CreatedBy == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId;
                }
                return ideas;
            }
            catch (Exception ex)
            {
                return new List<DataModel.BO.IdeasWatchDetail>();
            }
        }
        public static DataModel.BO.IdeasWatchDetail GetIdeasWatchDetail(int ideaWaatchId,string ideaCode="0")
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    var ideas = (from a in context.IdeasWatches
                                 where (a.IdeasWatchId == ideaWaatchId || a.IdeaCode == ideaCode ) && !a.IsDeleted
                                                                       select new DataModel.BO.IdeasWatchDetail
                                                                       {
                                                                           IdeaCode = a.IdeaCode,
                                                                           IdeasWatchId = a.IdeasWatchId,
                                                                           Title = a.Title,
                                                                           CurrentStatusName = a.IdeaStatu.Name,
                                                                           Description = a.Description,
                                                                           UserName = a.User.UserName,
                                                                           CreatedOn = a.CreatedOn,
                                                                           IsBug = a.IsBug,
                                                                           IdeaStatusId=a.IdeaStatusId,
                                                                           IdeaCategoryId = a.IdeaCategoryId,
                                                                           StatusChangedBy = a.User1.UserId,
                                                                           StatusChangedByName = a.User1.UserName,
                                                                           CommentDetailList = (from b in context.IdeaComments where b.IdeasWatchId == a.IdeasWatchId select new DataModel.BO.CommentDetail() { CommentId = b.CommentId, Comment = b.Comment, UserName = b.User.UserName, ImageUrl = b.User.ImageUrl, CreatedOn = b.CreatedOn, CommentReplyList = (from c in context.CommentReplies where c.CommentId == b.CommentId select new DataModel.BO.CommentReplyDetail { CommentReplyId=c.CommentReplyId, Detail = c.Detail, UserName = c.User.UserName, ImageUrl = c.User.ImageUrl, CreatedOn = c.CreatedOn }).ToList() }).ToList(),
                                                                           AttachmentList = a.FileAttachments.Where(b=>b.FileContent==null).ToList(), //context.FileAttachments.Where(b => b.IdeasWatchId == a.IdeasWatchId).Select(c => c.Url).ToList(),
                                                                           KeyList = context.IdeasWatch_Ideakey.Where(b => b.IdeasWatchId == a.IdeasWatchId).Select(c => c.IdeaKey).ToList(),
                                                                           KeyIdList = context.IdeasWatch_Ideakey.Where(b => b.IdeasWatchId == a.IdeasWatchId).Select(c => c.keyId).ToList(),
                                                                           LikedUserList = context.IdeaLikeDislikes.Where(b => b.IdeasWatchId == a.IdeasWatchId && b.IsLiked == true).Select(c => c.User.UserName).ToList(),
                                                                          // UnlikedUserList = context.IdeaLikeDislikes.Where(b => b.IdeasWatchId == a.IdeasWatchId && b.IsLiked == false).Select(c => c.User.UserName).ToList(),
                                                                           IsLikedByCuurentUser = context.IdeaLikeDislikes.Where(b => b.IdeasWatchId == a.IdeasWatchId && b.IsLiked == true && b.UserId == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId).ToList().Count > 0,
                                                                          // IsDislikedByCuurentUser = context.IdeaLikeDislikes.Where(b => b.IdeasWatchId == a.IdeasWatchId && b.IsLiked == false && b.UserId == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId).ToList().Count > 0,
                                                                           IsEditPermission = a.CreatedBy == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId,
                                                                           PageNameId = a.IdeaWatch_PageName.Select(b => b.PageNameId).FirstOrDefault()??0,
                                                                           ProductTypeId = a.IdeaWatch_ProductType.Select(b => b.ProductTypeId).FirstOrDefault() ?? 0,
                                                                           PlatformTypeId = a.IdeaWatch_PlatformType.Select(b => b.PlatformTypeId).FirstOrDefault() ?? 0,
                                                                           BusinessImpactId = a.IdeaWatch_BusinessImpact.Select(b => b.BusinessImpactId).FirstOrDefault() ?? 0,
                                                                           BusinessObjectiveId = a.IdeaWatch_BusinessObjective.Select(b => b.BusinessObjectiveId).FirstOrDefault() ?? 0,
                                                                           PageNameModel = a.IdeaWatch_PageName.Select(b => b.PageName).FirstOrDefault(),
                                                                           ProductTypeModel = context.IdeaWatch_ProductType.Where(b => b.IdeasWatchId == a.IdeasWatchId).Select(c => c.ProductType).FirstOrDefault(),     // (from b in  context.IdeaWatch_ProductType where b.IdeasWatchId==a.IdeasWatchId select b.ProductType).FirstOrDefault(),    // a.IdeaWatch_ProductType.Select(b => b.ProductType).FirstOrDefault(),
                                                                           PlatformTypeModel = a.IdeaWatch_PlatformType.Select(b => b.PlatformType).FirstOrDefault(),
                                                                           BusinessImpactModel = a.IdeaWatch_BusinessImpact.Select(b => b.BusinessImpact).FirstOrDefault(),
                                                                           BusinessObjectiveModel = a.IdeaWatch_BusinessObjective.Select(b => b.BusinessObjective).FirstOrDefault(),
                                                                           CategoryName = a.Category.Name,
                                                                           ReplicatedName = a.Replicated.Name,
                                                                           ReplicatedId = a.ReplicatedId,
                                                                           RelatedToName = a.RelatedTo.Name,
                                                                           RelatedToId = a.RelatedToId,
                                                                           CurrentScenario = a.CurrentScenario,
                                                                           BenifitIdea = a.BenifitIdea,
                                                                           NextStatusList = (from b in context.IdeaStatus where b.StepId == (a.IdeaStatu.StepId + 1) && a.IdeaStatusId != DataModel.Utility.Consts.IdeaStatus.Rejected select new DataModel.BO.NextStatus() { StatusId = b.IdeaStatusId, StatusName = b.Name }).ToList(),
                                                                           ParentId = a.IdeaStatu.ParentId ?? 0,
                                                                       }).FirstOrDefault();
                    return ideas;
                }
            }
            catch (Exception ex)
            {
                return new DataModel.BO.IdeasWatchDetail();
            }
        }
        public static List<DataModel.BO.CommentDetail> GetCommentsByIdeaWatchId(int ideaWaatchId)
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {

                    List<DataModel.BO.CommentDetail> CommentDetail = (from b in context.IdeaComments
                                                where b.IdeasWatchId == ideaWaatchId && !b.IdeasWatch.IsDeleted
                                                select new DataModel.BO.CommentDetail()
                                                {
                        CommentId = b.CommentId,
                        Comment = b.Comment,
                        UserName = b.User.UserName,
                        ImageUrl=b.User.ImageUrl,
                        CreatedOn = b.CreatedOn,
                        CommentReplyList = (from c in context.CommentReplies where c.CommentId == b.CommentId select new DataModel.BO.CommentReplyDetail { CommentReplyId=c.CommentReplyId, Detail = c.Detail, UserName = c.User.UserName, ImageUrl=c.User.ImageUrl, CreatedOn = c.CreatedOn }).ToList(),
                    
                    }).ToList();
                    return CommentDetail.ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
                return new List<DataModel.BO.CommentDetail>();
            }
        }
        public static List<DataModel.BO.CommentReplyDetail> GetCommentReplyByCommentId(int commentId)
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {

                    List<DataModel.BO.CommentReplyDetail> CommentDetail = (from b in context.CommentReplies where b.CommentId == commentId select new DataModel.BO.CommentReplyDetail() { CommentReplyId=b.CommentReplyId, Detail = b.Detail, ImageUrl = b.User.ImageUrl, UserName = b.User.UserName, CreatedOn = b.CreatedOn, }).ToList();
                    return CommentDetail.ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
                return new List<DataModel.BO.CommentReplyDetail>();
            }
        }
        public static List<DataModel.Model.IdeaKey> GetAllKey()
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.IdeaKey> ideas = (from a in context.IdeaKeys select a).ToList();
                    return ideas.ToList();
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.IdeaKey>();
            }
        }
        public static int GetTotalNumberOfIdeas()
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    int ideas = context.IdeasWatches.Where(a => !a.IsDeleted && a.IdeaStatusId == DataModel.Utility.Consts.IdeaStatus.Approved).Count();// (from a in context.IdeasWatches select a).Count();
                    return ideas;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static DataModel.Model.FileAttachment GetFileAttachment(int attachmentId)
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    var fileAttachment = (from a in context.FileAttachments where a.AttachmentId == attachmentId && a.FileContent == null select a).FirstOrDefault();
                    return fileAttachment;
                }
            }
            catch (Exception ex)
            {
                return new DataModel.Model.FileAttachment();
            }
        }
        public static List<DataModel.BO.KeyCloud> GetKeyCloudList()
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    var approvedIdeas = (from b in context.IdeasWatches where (b.IdeaStatusId == DataModel.Utility.Consts.IdeaStatus.Approved || b.IdeaStatu.ParentId == DataModel.Utility.Consts.IdeaStatus.Approved) && !b.IsDeleted select b.IdeasWatchId).ToList();
                    List<DataModel.BO.KeyCloud> keyCloudList = (from a in context.IdeaKeys
                                                                where a.IdeasWatch_Ideakey.Where(b => approvedIdeas.Contains(b.IdeasWatchId)).ToList().Count > 0
                                                              
                                                                 select new DataModel.BO.KeyCloud
                                                                 {
                                                                     KeyId=a.keyId,
                                                                     text=a.keyName,
                                                                     weight = a.IdeasWatch_Ideakey.Where(b => approvedIdeas.Contains(b.IdeasWatchId)).ToList().Count,
                                                                     link = "#",                                                                                                                                        
                                                                 }).ToList();
                   // List<DataModel.BO.KeyCloud> keyCloudList = keyCloud.ToList();
                    foreach (var key in keyCloudList)
                    {
                        key.link = "#" + Convert.ToString(key.KeyId);
                    }
                    return keyCloudList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.BO.KeyCloud>();
            }
        }       
        public static string GetKeyName(int keyId)
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    return (from a in context.IdeaKeys where a.keyId == keyId select a.keyName).FirstOrDefault();
                    
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<DataModel.Model.PlatformType> GetPlatformType()
        {
            try
            {
                List<DataModel.Model.PlatformType> resultCache = (List<DataModel.Model.PlatformType>)DataModel.Utility.Base.GetMyCachedItem(DataModel.Utility.Consts.CacheDetail.Cache_PlatformType);
                if (resultCache!=null)
                    return resultCache;
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.PlatformType> platformTypeList = (from a in context.PlatformTypes select a).ToList();
                    DataModel.Utility.Base.AddCache(DataModel.Utility.Consts.CacheDetail.Cache_PlatformType, platformTypeList);
                    return platformTypeList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.PlatformType>();
            }
        }
        public static List<DataModel.Model.PageName> GetPageName()
        {
            try
            {
                List<DataModel.Model.PageName> resultCache = (List<DataModel.Model.PageName>)DataModel.Utility.Base.GetMyCachedItem(DataModel.Utility.Consts.CacheDetail.Cache_PageName);
                if (resultCache != null)
                    return resultCache;
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.PageName> pageNamesList = (from a in context.PageNames select a).ToList();
                    DataModel.Utility.Base.AddCache(DataModel.Utility.Consts.CacheDetail.Cache_PageName, pageNamesList);
                    return pageNamesList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.PageName>();
            }
        }
        public static List<DataModel.Model.ProductType> GetProductType()
        {
            try
            {
                List<DataModel.Model.ProductType> resultCache = (List<DataModel.Model.ProductType>)DataModel.Utility.Base.GetMyCachedItem(DataModel.Utility.Consts.CacheDetail.Cache_ProductType);
                if (resultCache != null)
                    return resultCache;
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.ProductType> productTypeList = (from a in context.ProductTypes select a).ToList();
                   DataModel.Utility.Base.AddCache(DataModel.Utility.Consts.CacheDetail.Cache_ProductType, productTypeList);
                    return productTypeList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.ProductType>();
            }
        }
        public static List<DataModel.Model.BusinessObjective> GetBusinessObjective()
        {
            try
            {
                List<DataModel.Model.BusinessObjective> resultCache = (List<DataModel.Model.BusinessObjective>)DataModel.Utility.Base.GetMyCachedItem(DataModel.Utility.Consts.CacheDetail.Cache_BusinessObjective);
                if (resultCache != null)
                    return resultCache;
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.BusinessObjective> businessObjectiveList = (from a in context.BusinessObjectives select a).ToList();
                    DataModel.Utility.Base.AddCache(DataModel.Utility.Consts.CacheDetail.Cache_BusinessObjective, businessObjectiveList);
                    return businessObjectiveList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.BusinessObjective>();
            }
        }
        public static List<DataModel.Model.Category> GetCategory()
        {
            try
            {
                //List<DataModel.Model.Category> resultCache = (List<DataModel.Model.Category>)DataModel.Utility.Base.GetMyCachedItem(DataModel.Utility.Consts.CacheDetail.Cache_Category);
                //if (resultCache != null)
                //    return resultCache;
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.Category> categoryList = (from a in context.Categories select a).ToList();
                    DataModel.Utility.Base.AddCache(DataModel.Utility.Consts.CacheDetail.Cache_Category, categoryList);
                    return categoryList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.Category>();
            }
        }

        public static List<DataModel.Model.Location> GetLocation()
        {
            try
            {
                //List<DataModel.Model.Location> resultCache = (List<DataModel.Model.Location>)DataModel.Utility.Base.GetMyCachedItem(DataModel.Utility.Consts.CacheDetail.Cache_Location);
                //if (resultCache != null)
                //    return resultCache;
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.Location> locationList;
                    var chkLocation = (from b in context.Users where b.UserId == DataModel.Utility.CurrentUser.CurrentUserDetail.UserId select b).FirstOrDefault();
                    if (chkLocation.LocationId != null)
                    {
                        locationList = (from a in context.Locations where a.LocationId == chkLocation.LocationId select a).ToList();
                    }
                    else
                    {
                        locationList = (from a in context.Locations select a).ToList();
                    }
                    //DataModel.Utility.Base.AddCache(DataModel.Utility.Consts.CacheDetail.Cache_Location, locationList);
                    return locationList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.Location>();
            }
        }

        public static List<DataModel.Model.Replicated> GetReplicated()
        {
            try
            {
                //List<DataModel.Model.Replicated> resultCache = (List<DataModel.Model.Replicated>)DataModel.Utility.Base.GetMyCachedItem(DataModel.Utility.Consts.CacheDetail.Cache_Replicated);
                //if (resultCache != null)
                //    return resultCache;
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.Replicated> replicatedList = (from a in context.Replicateds select a).ToList();
                    DataModel.Utility.Base.AddCache(DataModel.Utility.Consts.CacheDetail.Cache_Replicated, replicatedList);
                    return replicatedList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.Replicated>();
            }
        }
        public static List<DataModel.Model.RelatedTo> GetRelatedTo()
        {
            try
            {
                //List<DataModel.Model.RelatedTo> resultCache = (List<DataModel.Model.RelatedTo>)DataModel.Utility.Base.GetMyCachedItem(DataModel.Utility.Consts.CacheDetail.Cache_RelatedTo);
                //if (resultCache != null)
                //    return resultCache;
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.RelatedTo> relatedToList = (from a in context.RelatedToes select a).ToList();
                    DataModel.Utility.Base.AddCache(DataModel.Utility.Consts.CacheDetail.Cache_RelatedTo, relatedToList);
                    return relatedToList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.RelatedTo>();
            }
        }
        public static List<DataModel.Model.Category> GetCategoryNotInUse()
        {
            try
            {
                
                using (var context = new FPIdeasWatchEntities())
                {
                    var categoryListItems = (from a in context.IdeasWatches select a.IdeaCategoryId).ToList();
                    List<DataModel.Model.Category> categoryList = (from a in context.Categories where !categoryListItems.Contains(a.CategoryId) select a).ToList();                 
                    return categoryList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.Category>();
            }
        }
        public static List<DataModel.Model.BusinessImpact> GetBusinessImpact()
        {
            try
            {
                List<DataModel.Model.BusinessImpact> resultCache = (List<DataModel.Model.BusinessImpact>)DataModel.Utility.Base.GetMyCachedItem(DataModel.Utility.Consts.CacheDetail.Cache_BusinessImpact);
                if (resultCache != null)
                    return resultCache;
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.BusinessImpact> businessImpactList = (from a in context.BusinessImpacts select a).ToList();
                  DataModel.Utility.Base.AddCache(DataModel.Utility.Consts.CacheDetail.Cache_BusinessImpact, businessImpactList);
                    return businessImpactList;
                }
            }
            catch (Exception ex)
            {
                return new List<DataModel.Model.BusinessImpact>();
            }
        }

        public static bool DeleteIdea(int ideaWatchId)
        {
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {
                    var idea = (from a in context.IdeasWatches where a.IdeasWatchId == ideaWatchId select a).FirstOrDefault();

                    //var allAtachments = (from a in context.FileAttachments where a.IdeasWatchId == idea.IdeasWatchId select a).ToList();
                    //foreach (var item in allAtachments)
                    //{
                    //    context.FileAttachments.Remove(item);
                    //    context.SaveChanges();
                    //}
                    //var ideaLike = (from a in context.IdeaLikeDislikes where a.IdeasWatchId == idea.IdeasWatchId select a).ToList();
                    //foreach (var item in ideaLike)
                    //{
                    //    context.IdeaLikeDislikes.Remove(item);
                    //    context.SaveChanges();
                    //}
                    //var comments = (from a in context.IdeaComments where a.IdeasWatchId == idea.IdeasWatchId select a).ToList();
                    //foreach (var item in comments)
                    //{
                    //    var commentReplies = (from b in context.CommentReplies where b.CommentId == item.CommentId select b).ToList();
                    //    foreach(var reply in commentReplies)
                    //    {
                    //        context.CommentReplies.Remove(reply);

                    //    }
                    //    context.SaveChanges();
                    //    context.IdeaComments.Remove(item);
                    //    context.SaveChanges();
                    //}
                    //var ideakeyAss = (from a in context.IdeasWatch_Ideakey where a.IdeasWatchId == idea.IdeasWatchId select a).ToList();
                    //foreach (var item in ideakeyAss)
                    //{
                    //    context.IdeasWatch_Ideakey.Remove(item);
                    //    context.SaveChanges();
                    //}

                    //var ideaWatch_PageName = (from a in context.IdeaWatch_PageName where a.IdeasWatchId == idea.IdeasWatchId select a).ToList();
                    //foreach (var item in ideaWatch_PageName)
                    //{
                    //    context.IdeaWatch_PageName.Remove(item);
                    //    context.SaveChanges();
                    //}
                    //var ideaWatch_PlatformType = (from a in context.IdeaWatch_PlatformType where a.IdeasWatchId == idea.IdeasWatchId select a).ToList();
                    //foreach (var item in ideaWatch_PlatformType)
                    //{
                    //    context.IdeaWatch_PlatformType.Remove(item);
                    //    context.SaveChanges();
                    //}
                    //var ideaWatch_ProductType = (from a in context.IdeaWatch_ProductType where a.IdeasWatchId == idea.IdeasWatchId select a).ToList();
                    //foreach (var item in ideaWatch_ProductType)
                    //{
                    //    context.IdeaWatch_ProductType.Remove(item);
                    //    context.SaveChanges();
                    //}
                    //var ideaWatch_BusinessImpact = (from a in context.IdeaWatch_BusinessImpact where a.IdeasWatchId == idea.IdeasWatchId select a).ToList();
                    //foreach (var item in ideaWatch_BusinessImpact)
                    //{
                    //    context.IdeaWatch_BusinessImpact.Remove(item);
                    //    context.SaveChanges();
                    //}
                    //var ideaWatch_BusinessObjective = (from a in context.IdeaWatch_BusinessObjective where a.IdeasWatchId == idea.IdeasWatchId select a).ToList();
                    //foreach (var item in ideaWatch_BusinessObjective)
                    //{
                    //    context.IdeaWatch_BusinessObjective.Remove(item);
                    //    context.SaveChanges();
                    //}

                    idea.IsDeleted = true;                  
                   // context.IdeasWatches.Remove(idea);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool DeleteComment(int commentId)
        {
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {
                    var comment = (from a in context.IdeaComments where a.CommentId == commentId select a).FirstOrDefault();

                    var commentReplies = (from b in context.CommentReplies where b.CommentId == comment.CommentId select b).ToList();
                    foreach (var reply in commentReplies)
                    {
                        context.CommentReplies.Remove(reply);

                    }
                    context.IdeaComments.Remove(comment);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public static bool DeleteCommentReply(int commentReplyId)
        {
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {

                    var reply = (from b in context.CommentReplies where b.CommentReplyId == commentReplyId select b).FirstOrDefault();
                    context.CommentReplies.Remove(reply);                   
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteCategory(int categoryId)
        {
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {

                    var category = (from b in context.Categories where b.CategoryId == categoryId select b).FirstOrDefault();
                    context.Categories.Remove(category);
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static DataModel.BO.ServiceReturnType.ServiceResult<int> UpdateIdeaWatch(DataModel.BO.IdeasWatchDetail ideaWatchDeatil)
        {
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {
                    var idea = (from a in context.IdeasWatches where a.IdeasWatchId == ideaWatchDeatil.IdeasWatchId && !a.IsDeleted select a).FirstOrDefault();
                   
                    if(idea!=null)
                    {
                        int ideaWatchId = idea.IdeasWatchId;
                        //DataModel.Model.IdeaComment comment=new IdeaComment();
                        //comment.Comment = "<p><h4 style='color:green;font-weight:bold;'> The below idea's description has been updated !</h4></p>" + idea.Description;
                        //comment.IdeasWatchId = ideaWatchDeatil.IdeasWatchId;
                        //comment.UserId = DataModel.Utility.CurrentUser.CurrentUserDetail.UserId;
                        //comment.CreatedOn = DataModel.Utility.Base.GetCurrentIndianDateTIme(); 
                        //AddIdeaComments(comment);
                        idea.Description = ideaWatchDeatil.Description;
                        idea.IsBug = ideaWatchDeatil.IsBug;
                        idea.ModifiedOn = DataModel.Utility.Base.GetCurrentIndianDateTIme();
                        idea.IdeaCategoryId = ideaWatchDeatil.IdeaCategoryId;
                        idea.ReplicatedId = ideaWatchDeatil.ReplicatedId;
                        idea.RelatedToId = ideaWatchDeatil.RelatedToId;
                        idea.CurrentScenario = ideaWatchDeatil.CurrentScenario;
                        idea.BenifitIdea = ideaWatchDeatil.BenifitIdea;
                        var result1 = context.IdeaWatch_BusinessImpact.Where(a => a.IdeasWatchId == ideaWatchId).Select(a => a).FirstOrDefault();
                        if (ideaWatchDeatil.BusinessImpactId > 0)
                        {
                            if (result1 == null)
                            {
                                DataModel.Model.IdeaWatch_BusinessImpact ideaWatch_BusinessImpact = new IdeaWatch_BusinessImpact();
                                ideaWatch_BusinessImpact.IdeasWatchId = ideaWatchId;
                                ideaWatch_BusinessImpact.BusinessImpactId = ideaWatchDeatil.BusinessImpactId;
                                context.IdeaWatch_BusinessImpact.Add(ideaWatch_BusinessImpact);
                            }
                            else
                            {
                                result1.BusinessImpactId = ideaWatchDeatil.BusinessImpactId;

                            }
                        }
                        else
                        {
                            if (result1 != null)
                            {
                                context.IdeaWatch_BusinessImpact.Remove(result1);
                            }

                        }

                        var result2 = context.IdeaWatch_PlatformType.Where(a => a.IdeasWatchId == ideaWatchId).Select(a => a).FirstOrDefault();
                        if (ideaWatchDeatil.PlatformTypeId > 0)
                        {
                            if (result2 == null)
                            {
                                DataModel.Model.IdeaWatch_PlatformType ideaWatch_PlatformType = new IdeaWatch_PlatformType();
                                ideaWatch_PlatformType.IdeasWatchId = ideaWatchId;
                                ideaWatch_PlatformType.PlatformTypeId = ideaWatchDeatil.PlatformTypeId;
                                context.IdeaWatch_PlatformType.Add(ideaWatch_PlatformType);
                            }
                            else
                            {
                                result2.PlatformTypeId = ideaWatchDeatil.PlatformTypeId;
                            }
                        }
                        else
                        {
                            if (result2 != null)
                            {
                                context.IdeaWatch_PlatformType.Remove(result2);
                            }

                        }
                        var result3 = context.IdeaWatch_ProductType.Where(a => a.IdeasWatchId == ideaWatchId).Select(a => a).FirstOrDefault();
                        if (ideaWatchDeatil.ProductTypeId > 0)
                        {
                            if (result3 == null)
                            {
                                DataModel.Model.IdeaWatch_ProductType ideaWatch_ProductType = new IdeaWatch_ProductType();
                                ideaWatch_ProductType.IdeasWatchId = ideaWatchId;
                                ideaWatch_ProductType.ProductTypeId = ideaWatchDeatil.ProductTypeId;
                                context.IdeaWatch_ProductType.Add(ideaWatch_ProductType);
                            }
                            else {
                                result3.ProductTypeId = ideaWatchDeatil.ProductTypeId;
                            }
                        }
                        else
                        {
                            if (result3 != null)
                            {
                                context.IdeaWatch_ProductType.Remove(result3);
                            }

                        }
                        var result4 = context.IdeaWatch_PageName.Where(a => a.IdeasWatchId == ideaWatchId).Select(a => a).FirstOrDefault();
                        if (ideaWatchDeatil.PageNameId > 0)
                        {
                            if (result4 == null)
                            {
                                DataModel.Model.IdeaWatch_PageName ideaWatch_PageName = new IdeaWatch_PageName();
                                ideaWatch_PageName.IdeasWatchId = ideaWatchId;
                                ideaWatch_PageName.PageNameId = ideaWatchDeatil.PageNameId;
                                context.IdeaWatch_PageName.Add(ideaWatch_PageName);
                            }
                            else
                            {
                                result4.PageNameId = ideaWatchDeatil.PageNameId;
                            }
                        }
                        else
                        {
                            if (result4 != null)
                            {
                                context.IdeaWatch_PageName.Remove(result4);
                            }

                        }
                        var result5 = context.IdeaWatch_BusinessObjective.Where(a => a.IdeasWatchId == ideaWatchId).Select(a => a).FirstOrDefault();
                        if (ideaWatchDeatil.BusinessObjectiveId > 0)
                        {
                            if (result5 == null)
                            {
                                DataModel.Model.IdeaWatch_BusinessObjective ideaWatch_BusinessObjective = new IdeaWatch_BusinessObjective();
                                ideaWatch_BusinessObjective.IdeasWatchId = ideaWatchId;
                                ideaWatch_BusinessObjective.BusinessObjectiveId = ideaWatchDeatil.BusinessObjectiveId;
                                context.IdeaWatch_BusinessObjective.Add(ideaWatch_BusinessObjective);
                            }
                            else {
                                result5.BusinessObjectiveId = ideaWatchDeatil.BusinessObjectiveId;
                            }
                        }
                        else
                        {
                            if (result5 != null)
                            {
                                context.IdeaWatch_BusinessObjective.Remove(result5);
                            }

                        }




                    }                   
                    context.SaveChanges();
                    return DataModel.BO.ServiceReturnType.SuccessWithDataResult<int>(ideaWatchDeatil.IdeasWatchId);
                }
            }
            catch (Exception ex)
            {
                var result = DataModel.BO.ServiceReturnType.FailureWithDataResult<int>(0);
                result.Message = "Error !" + ex.Message + " Inner Exception !" + ex.InnerException;
                return result;
            }
        }

        public static int AddNewCategory(DataModel.Model.Category obj)
        {
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {
                    context.Categories.Add(obj);
                    context.SaveChanges();
                    return obj.CategoryId;
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
       
       
    }
}
