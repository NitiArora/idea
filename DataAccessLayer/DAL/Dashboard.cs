﻿using DataModel.BO;
using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DAL
{
    public class Dashboard
    {
        public static List<ChartView> GetIdeaStatusData()
        {
           
            List<ChartView> resultList = new List<ChartView>();
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    List<DataModel.Model.IdeasWatch> listIdea = new List<DataModel.Model.IdeasWatch>();
                    if (DataModel.Utility.CurrentUser.CurrentUserDetail.IsSuperAdmin || DataModel.Utility.CurrentUser.CurrentUserDetail.IsReviewPanelMember)
                    { listIdea = (from a in context.IdeasWatches where !a.IsDeleted select a).ToList(); }
                    else
                    {
                        listIdea = (from a in context.IdeasWatches where !a.IsDeleted && a.CreatedBy==DataModel.Utility.CurrentUser.CurrentUserDetail.UserId select a).ToList();
                    }
                    var ideaStatus = (from a in context.IdeaStatus select a).ToList();

                    foreach(var idea in ideaStatus)
                    {
                        ChartView chartData = new ChartView();
                        chartData.Id = idea.IdeaStatusId;
                        chartData.Name = idea.Name;
                        chartData.Value = listIdea.Where(a => a.IdeaStatusId == idea.IdeaStatusId).Count();
                        resultList.Add(chartData);
                    }
                 
                  
                   //ChartView result2 = new ChartView();
                   //result2.Name = "Rejected";
                   //result2.Value =  result.Where(a => a.IdeaStatusId == DataModel.Utility.Consts.IdeaStatus.Rejected).Count();
                   //resultList.Add(result2);
                   //ChartView result3 = new ChartView();
                   //result3.Name = "Pending";
                   //result3.Value = result.Where(a => a.IdeaStatusId == DataModel.Utility.Consts.IdeaStatus.Pending).Count();
                   //resultList.Add(result3);                             
                   return resultList;
                }
            }
            catch (Exception ex)
            {
               
                return resultList;
            }
        }
    }
}
