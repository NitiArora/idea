﻿using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DAL
{
    public class ManageIdeaDAL
    {
        public static DataModel.BO.ServiceReturnType.ServiceResult<DataModel.BO.IdeasWatchDetail> ChangeIdeaStatus(int ideaId, int ideaStatusId)
        {
            DataModel.BO.IdeasWatchDetail obj = new DataModel.BO.IdeasWatchDetail();
            try
            {
                DataModel.Utility.Base.ResetCache();
                using (var context = new FPIdeasWatchEntities())
                {

                    var exist = (from a in context.IdeasWatches where a.IdeasWatchId == ideaId select a).FirstOrDefault();
                    if (exist != null)
                    {
                        if (exist.IdeaStatusId != ideaStatusId)
                        {
                            exist.IdeaStatusId = ideaStatusId;
                            exist.StatusChangedBy = DataModel.Utility.CurrentUser.CurrentUserDetail.UserId;
                            context.SaveChanges();
                            obj.Title = exist.Title;
                            obj.IdeaCode = exist.IdeaCode;
                            obj.Status = exist.IdeaStatu.Name;
                            obj.CreatedByEmail = (from a in context.Users where a.UserId == exist.CreatedBy select a.UserName).FirstOrDefault();
                        }
                        else
                        {
                            var result = DataModel.BO.ServiceReturnType.FailureWithDataResult<DataModel.BO.IdeasWatchDetail>(obj);
                            result.Message = "Status is already changed by : " +exist.User1.UserName ;
                            return result; // Status is already changed
                        }
                    }

                    return DataModel.BO.ServiceReturnType.SuccessWithDataResult<DataModel.BO.IdeasWatchDetail>(obj);
                }
            }
            catch (Exception ex)
            {
                return DataModel.BO.ServiceReturnType.FailureWithDataResult<DataModel.BO.IdeasWatchDetail>(obj);
            }
        }
    }
}
