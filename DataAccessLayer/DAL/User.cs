﻿using DataModel.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccessLayer.DAL
{
    public class User
    {
        public static DataModel.BO.UserDetail AddUser(string UserName, string imageUrl)
        {
            DataModel.BO.UserDetail user = new DataModel.BO.UserDetail();
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    string trimUserName = UserName.Trim();
                    var existUser = (from a in context.Users where a.UserName == trimUserName select a).FirstOrDefault();
                    if (existUser == null)
                    {
                        DataModel.Model.User obj = new DataModel.Model.User();
                        obj.UserName = trimUserName;
                        obj.ImageUrl = imageUrl;
                        obj.CreatedOn = DateTime.Now;
                        context.Users.Add(obj);
                        context.SaveChanges();
                        user.UserId = obj.UserId;
                        user.IsSuperAdmin = false;
                        user.IsReviewPanelMember = false;
                        user.LocationId = obj.LocationId;

                    }
                    else
                    {
                        existUser.ImageUrl = imageUrl;
                        context.SaveChanges();
                        user.UserId = existUser.UserId;
                        user.IsSuperAdmin = existUser.UserRoles.Any(a => a.RoleId == DataModel.Utility.Consts.Role.SuperAdminId);
                        user.IsReviewPanelMember = existUser.UserRoles.Any(a => a.RoleId == DataModel.Utility.Consts.Role.ReviewPanelId);
                        user.LocationId = existUser.LocationId;

                    }
                    return user;
                }
            }
            catch (Exception ex)
            {

                return user;
            }
        }
        public static bool AssignRole(int[] userId, int roleId)
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    foreach (var v in userId)
                    {
                        context.UserRoles.Add(new UserRole() { UserId = v, RoleId = roleId });
                    }
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool RemoveRole(int userId)
        {
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {
                    var exist = (from a in context.UserRoles where a.UserId == userId select a).FirstOrDefault();
                    if (exist != null)
                    {
                        context.UserRoles.Remove(exist);
                        context.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static List<DataModel.BO.UserDetail> GetUserOfReviewPanel()
        {
            List<DataModel.BO.UserDetail> user = new List<DataModel.BO.UserDetail>();
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {

                     user = (from a in context.Users
                                     where a.UserRoles.Any(b => b.RoleId == DataModel.Utility.Consts.Role.ReviewPanelId)
                                     orderby a.UserName
                                     select new DataModel.BO.UserDetail
                                     {
                                         UserId = a.UserId,
                                         UserName = a.UserName,
                                         ImageUrl = a.ImageUrl,

                                     }).ToList();

                    return user;
                }
            }
            catch (Exception ex)
            {

                return user;
            }
        }

        public static List<string> GetEmailListOfReviewPanel() 
        {
            List<string> user = new List<string>();
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {

                    user = (from a in context.Users
                            where a.UserRoles.Any(b => b.RoleId == DataModel.Utility.Consts.Role.ReviewPanelId)
                            orderby a.UserName
                            select a.UserName
                            ).ToList();
                    return user;
                }
            }
            catch (Exception ex)
            {
                return user;
            }
        }
        public static List<DataModel.BO.UserDetail> GetUserWithoutReviewPanel()
        {
            List<DataModel.BO.UserDetail> user = new List<DataModel.BO.UserDetail>();
            try
            {
                using (var context = new FPIdeasWatchEntities())
                {

                    user = (from a in context.Users
                            where !a.UserRoles.Any(b => b.RoleId == DataModel.Utility.Consts.Role.ReviewPanelId) && !a.UserRoles.Any(b => b.RoleId == DataModel.Utility.Consts.Role.SuperAdminId)
                            orderby a.UserName
                            select new DataModel.BO.UserDetail
                            {
                                UserId = a.UserId,
                                UserName = a.UserName,
                                ImageUrl = a.ImageUrl,

                            }).ToList();

                    return user;
                }
            }
            catch (Exception ex)
            {

                return user;
            }
        }
    }
}
